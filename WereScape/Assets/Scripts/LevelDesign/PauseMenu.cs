﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    [SerializeField] private GameObject pauseMenuUI;        // Le panel de pause à activer / désactiver
    [SerializeField] private GameObject[] weapons;          // Les armes à activer / désactiver

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            GlobalVariables.isPaused = !GlobalVariables.isPaused;
        }

        if(GlobalVariables.isPaused)
        {
            ActivateMenu();
        }
        else
        {
            DeactivateMenu();
        }
    }

    public void ActivateMenu()
    {
        for (int i = 0; i < weapons.Length; i++)
        {
            weapons[i].SetActive(false);
        }
        Time.timeScale = 0;
        AudioListener.pause = true;
        pauseMenuUI.SetActive(true);
        //GlobalVariables.isPaused = true;
    }

    public void DeactivateMenu()
    {
        for (int i = 0; i < weapons.Length; i++)
        {
            weapons[i].SetActive(true);
        }
        Time.timeScale = 1;
        AudioListener.pause = false;
        pauseMenuUI.SetActive(false);
        GlobalVariables.isPaused = false;
    }

    public void Quitter()
    {
        Application.Quit();
    }
}
