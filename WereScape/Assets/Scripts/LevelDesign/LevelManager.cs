﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public LevelChanger levelChanger;   // Référence sur le levelChanger pour pouvoir appeler ses fonctions

    public void FadeToNextLevel()
    {
        levelChanger.FadeToLevel(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void FadeToLevel(int levelIndex)
    {
        levelChanger.FadeToLevel(levelIndex);
    }

}
