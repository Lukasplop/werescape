﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class SpawnerController : MonoBehaviour
{
    public int numberOfScientistToSpawn;               // Le nombre de scientifique qu'on veut faire spawn
    private int numberOfScientistLeftToSpawn;          // Le nombre de scientifique qu'il reste à faire spawn
    public GameObject scientist;                       // La reference sur l'objet scientifique 
    private int numberOfScientistLeftToKill;           // Le nombre de scientifique qu'il reste à tuer, decrease quand on scientifique meurt

    public int numberOfMilitaryToSpawn;               // Le nombre de militaire qu'on veut faire spawn
    private int numberOfMilitaryLeftToSpawn;          // Le nombre de militaire qu'il reste à faire spawn
    public GameObject military;                       // La reference sur l'objet militaire 
    private int numberOfMilitaryLeftToKill;           // Le nombre de militaire qu'il reste à tuer, decrease quand on militaire meurt

    public int maxSimultaneousEnnemies;               // Le nombre d'ennemis max en simultané
    private int currentSimultaneousEnnemies;          // Le nombre actuel d'ennemis en simultané

    public float startTimeBeforeNextLevel = 1;        // Le temps après avoir tué tous les ennemis avant de passer au niveau suivant
    private float timeBeforeNextLevel;                // Le timer actuel avant le niveau suivant


    // Multiplicateurs pour les différentes formes
    private PlayerController player;
    public float humanMultiplier = 1;
    public float youngWolfMultiplier = 1.3f;
    public float wolfMultiplier = 1.6f;
    public float alphaMultiplier = 4;
    private float currentMultiplier;
    private bool goOn = false;

    public Transform[] spawnSpots;                     // Un tableau contenant toutes les positions de spawn
    private float timeBtwSpawns;                       // Le temps entre chaque spawn, reprend la valeur de startTimeBtwSpawns à chaque itération
    public float startTimeBtwSpawns;                   // Le temps entre chaque spawn 
    private int randEnnemy;                            // Pour invoquer un type d'ennemi random

    public LevelManager levelManager;                  // Référence sur le LevelManager pour charger le niveau suivant lorsqu'il n'y a plus d'ennemis

    // La fonction ModifyDifficulty() est appelée dans le TransformationController lorsque l'on sait sous quelle forme
    // le joueur va jouer le niveau. Elle permet de modifier dynamique la difficulté en fonction de la forme du joueur
    public void ModifyDifficulty()
    {
        // Récupération de la forme actuelle et mise à jour du currentMultiplier
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        if (player.forme == "Human")
            currentMultiplier = humanMultiplier;
        else if (player.forme == "YoungWolf")
            currentMultiplier = youngWolfMultiplier;
        else if (player.forme == "Wolf")
            currentMultiplier = wolfMultiplier;
        else if (player.forme == "ALPHA")
            currentMultiplier = alphaMultiplier;

        // Utilisation du currentMultiplier pour modifier la difficulté du jeu
        numberOfScientistLeftToSpawn = (int)Mathf.Ceil(numberOfScientistToSpawn * currentMultiplier);
        numberOfScientistLeftToKill = (int)Mathf.Ceil(numberOfScientistToSpawn * currentMultiplier);
        numberOfMilitaryLeftToSpawn = (int)Mathf.Ceil(numberOfMilitaryToSpawn * currentMultiplier);
        numberOfMilitaryLeftToKill = (int)Mathf.Ceil(numberOfMilitaryToSpawn * currentMultiplier);
        maxSimultaneousEnnemies = (int)Mathf.Ceil(maxSimultaneousEnnemies * currentMultiplier);
        startTimeBtwSpawns = startTimeBtwSpawns / currentMultiplier;

        // Initialisation des timers
        timeBtwSpawns = startTimeBtwSpawns;
        timeBeforeNextLevel = startTimeBeforeNextLevel;

        goOn = true;
    }

    private void Update()
    {
        // Si la difficulté à été modifiée
        if (goOn)
        {
            // Si il reste des ennemis à faire spawn et que le max d'ennemis en même temps n'a pas été atteint
            if (numberOfScientistLeftToSpawn + numberOfMilitaryLeftToSpawn > 0 && currentSimultaneousEnnemies < maxSimultaneousEnnemies)
            {
                // Si le temps entre deux spawns est écoulé
                if (timeBtwSpawns <= 0)
                {
                    // On choisit une position aléatoire parmis les positions où les ennemis peuvent spawn
                    int randPos = Random.Range(0, spawnSpots.Length);

                    if (numberOfScientistLeftToSpawn > 0 && numberOfMilitaryLeftToSpawn > 0)    // Si il peut encore y avoir des deux types d'ennemis, alors le type est aléatoire
                    {
                        randEnnemy = Random.Range(0, 2);
                    }
                    else if (numberOfScientistLeftToSpawn <= 0)                                 // Si il n'y a plus de scientifique, alors on fait spawn un militaire
                    {
                        randEnnemy = 0;
                    }
                    else if (numberOfMilitaryLeftToSpawn <= 0)                                  // Si il n'y a plus de militaire, alors on fait spawn un scientifique
                    {
                        randEnnemy = 1;
                    }

                    if (randEnnemy == 0)
                    {
                        Instantiate(military, spawnSpots[randPos].position, Quaternion.identity);
                        numberOfMilitaryLeftToSpawn--;
                    }
                    else if (randEnnemy == 1)
                    {
                        Instantiate(scientist, spawnSpots[randPos].position, Quaternion.identity);
                        numberOfScientistLeftToSpawn--;
                    }
                    timeBtwSpawns = startTimeBtwSpawns;
                    currentSimultaneousEnnemies += 1;
                }
                else
                {
                    timeBtwSpawns -= Time.deltaTime;
                }
            }

            // Timer permettant de faire un fondu entre les niveaux lorsqu'on a terminé celui ci
            if (timeBeforeNextLevel < startTimeBeforeNextLevel)
            {
                timeBeforeNextLevel -= Time.deltaTime;
                if (timeBeforeNextLevel <= 0)
                {
                    NextLevel();
                }
            }
        }
    }

    public void DeathOfScientist()
    {
        numberOfScientistLeftToKill--;
        currentSimultaneousEnnemies -= 1;
        if (numberOfMilitaryLeftToKill + numberOfScientistLeftToKill == 0)
        {
            timeBeforeNextLevel -= Time.deltaTime;
        }
    }

    public void DeathOfMilitary()
    {
        numberOfMilitaryLeftToKill--;
        currentSimultaneousEnnemies -= 1;
        if (numberOfMilitaryLeftToKill + numberOfScientistLeftToKill == 0)
        {
            timeBeforeNextLevel -= Time.deltaTime;
        }
    }

    public void NextLevel()
    {
        player.FadeToZero();
        levelManager.FadeToNextLevel();
    }
}

