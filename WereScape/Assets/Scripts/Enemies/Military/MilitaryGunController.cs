﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MilitaryGunController : MonoBehaviour
{
    public GameObject shot;                 // Reference sur l'objet projectile qu'on va envoyer
    public GameObject firingPoint;          // La position de depart du projectile (le bout du pistolet)
    public GameObject gunPivot;             // La référence sur l'objet gunPivot

    private AudioSource gunshot;            // Le bruitage lorsqu'on tire au pistolet

    private void Start()
    {
        gunshot = GetComponentInChildren<AudioSource>();
    }

    public void Shot()
    {
        gunshot.Play();
        Instantiate(shot, firingPoint.transform.position, gunPivot.transform.rotation);
    }
}
