﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationMilitaryGunController : MonoBehaviour
{
    private Vector2 direction;              // La direction vers laquelle est la souris en partant du pistolet

    public float rightSideXPos;             // La position en x du pivot de droite
    public float leftSideXPos;              // la position en x du pivot de gauche
    public float yPos;                      // la position en y des pivots

    public GameObject gun;                  // Référence sur le GameObject du pistolet du militaire
    public Transform playerPos;             // Référence sur la position du joueur
    public Transform militaryPos;           // Référence sur la position du militaire

    private Vector2 rightPos;               // Position de la main droite du militaire
    private Vector2 leftPos;                // Position de la main gauche du militaire

    private bool onRightSide = true;        // Le side actuel du pistolet            

    private void Start()
    {
        // Création des positions
        rightPos = new Vector2(rightSideXPos, yPos);
        leftPos = new Vector2(leftSideXPos, yPos);

        // Récupération des références
        playerPos = GameObject.FindGameObjectWithTag("Player").transform;
        militaryPos = gameObject.GetComponentInParent<Transform>();
    }

    void FixedUpdate()
    {
        // On calcule la direction, et en fonction on change le côté du pistolet
        direction = -((Vector2)playerPos.position - (Vector2)militaryPos.position).normalized;
        if (direction.x < -0.2 && !onRightSide)
        {
            GoRightSide();
        }
        else if (direction.x >= 0.2 && onRightSide)
        {
            GoLeftSide();
        }

        // et on modifie directement le transform pour faire en sorte que le pistolet "regarde" le joueur
        transform.up = direction;
    }

    private void GoLeftSide()
    {
        transform.localPosition = leftPos;
        RotateGun();
        onRightSide = false;
    }

    private void GoRightSide()
    {
        transform.localPosition = rightPos;
        RotateGun();
        onRightSide = true;
    }

    private void RotateGun()
    {
        var rot = gun.transform.rotation;
        gun.transform.rotation = rot * Quaternion.Euler(0, 180, 0);

        gun.transform.localPosition = new Vector2(-gun.transform.localPosition.x, gun.transform.localPosition.y);
    }
}

