﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MillitaryBulletControler : MonoBehaviour
{
    private Rigidbody2D rb;         // Le RigidBody du projectile
    private Vector2 direction;      // La direction que va prendre le projectile

    public float speed;             // La vitesse du projectile
    public float lifetime = 3f;     // La duree de vie
    public int damage;              // Les dégats du projectile

    private GameObject player;      // Référence sur le joueur pour lui enlever de la vie

    private void Start()
    {
        // Récupération des références
        player = GameObject.FindGameObjectWithTag("Player");
        rb = gameObject.GetComponentInChildren<Rigidbody2D>();

        // Calcul de la direction et de la vitesse
        direction = ((Vector2)player.transform.position - (Vector2)transform.position).normalized;
        rb.velocity = direction * speed;
    }

    private void Update()
    {
        lifetime -= Time.deltaTime;
        if (lifetime <= 0f)
        {
            Die();
        }
    }

    public void Die()
    {
        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if (player == null)
                player = GameObject.FindGameObjectWithTag("Player");
            player.GetComponent<PlayerController>().GetHit(damage);
            Die();
        }
        if (collision.CompareTag("Wall") || collision.CompareTag("Obstacle"))
        {
            Die();
        }
    }
}
