﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class MilitaryController : MonoBehaviour
{
    public int health;                               // La vie du militaire
    public float speed;                              // La vitesse du militaire
    public float stoppingDistance;                   // Distance d'arrêt du joueur
    public float retreatDistance;                    // Distance à laquelle il commence à reculer
    public float shootingDistance;                   // Distance max de tir
    public GameObject deathEffect;                   // Effet à la mort stylé ou quoi
    private PlayerController player;                 // Le script du joueur 

    private SpawnerController spawner;               // La référence sur le script du spawner pour pouvoir faire baisser le nombre de militaire qu'il reste quand il meurt
    public AudioSource hitSound;

    private float timeBtwShots;                      // Temps entre les tirs, prend la valeur de startTimeBtwShots
    public float startTimeBtwShots;

    // Pour les piles
    public int batteryDropRate;                      // Les chances pour drop une pile sont de 1/batteryDropRate
    public GameObject battery;                       // Une référence sur le gameObject de la pile

    private Vector2 direction;                       // La direction vers laquelle regarde (position du joueur)
    private bool moving;                             // Pour savoir si le militaire bouge
    private bool dead = false;
    private bool isDidacticiel = false;

    // Pour empêcher de prendre 2 fois les dégats vu qu'il a 2 collider
    private float timeBtwGettingHit;
    private float startTimeBtwGettingHit = 0.1f;

    public float startTimeBeforeDying;               // Le temps avant qu'il meure à 0hp
    private float timeBeforeDying = 0;               // Le temps avant qu'il meure

    public GameObject gun;                           // Une référence sur le pistolet du militaire pour pouvoir tirer
    private MilitaryGunController gunController;     // Une référence sur le script de son pistolet 
    public GameObject projectile;                    // Une référence sur le projectile tiré par le militaire
    private SpriteRenderer spriteRenderer;           // Pour le passage en rouge quand il se fait toucher
    public Animator animator;                        // L'animateur de notre objet

    private Transform playerPos;                     // La position du joueur
    private GameObject[] walls;                      // Référence sur tous les murs de l'arène
    private Rigidbody2D rb;

    private RaycastHit2D[] hits;                     // Pour détecter si y'a un mur entre le militaire et le joueur

    //A*
    public float nextWaypointDistance = 1f;          // distance avant de passer au waypoint suivant
    private Path path;                               // Une variable qui contient le chemin actuel
    private int currentWaypoint = 0;                 // index du waypoint actuel
    private Seeker seeker;                           // référence sur le component seeker de l'objet
    public float updatePathRate = 0.1f;              // Temps entre chaque rafraichissement des chemins

    void Start()
    {
        // Récupération des références
        gunController = gun.GetComponent<MilitaryGunController>();
        playerPos = GameObject.FindGameObjectWithTag("Player").transform;
        spawner = GameObject.FindGameObjectWithTag("Spawner").GetComponent<SpawnerController>();
        animator = GetComponent<Animator>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        rb = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        seeker = GetComponent<Seeker>();

        // Initialisation des timers
        timeBtwShots = startTimeBtwShots;
        timeBtwGettingHit = startTimeBtwGettingHit;

        //On récupère les murs et on ignore les collisions avec eux pour pouvoir passer à travers les portes
        walls = GameObject.FindGameObjectsWithTag("Wall");
        for (int i = 0; i < walls.Length; i++)
        {
            Physics2D.IgnoreCollision(walls[i].GetComponent<Collider2D>(), gameObject.GetComponent<Collider2D>());
        }


        InvokeRepeating("UpdatePath", 0f, updatePathRate);     // Permet de lancer la fonction UpdatePath tous les updatePathRate

        // Pour que les ennemis ne poussent pas le joueur
        Physics2D.IgnoreCollision(GameObject.FindGameObjectWithTag("Player").GetComponent<Collider2D>(), gameObject.GetComponent<Collider2D>());

        // On vérifie si ce sont les ennemis du didacticiel
        if (transform.parent != null)
            if (transform.parent.name == "Ennemies")
               isDidacticiel = true;
    }

    void FixedUpdate()
    {
        // Si le joueur n'est pas nul et que le militaire n'est pas mort, alors on se déplace vers le joueur
        if (playerPos != null && dead == false)
        {
            moving = true;

            // Calcul de la direction vers laquelle le joueur se trouve
            direction = -((Vector2)playerPos.position - (Vector2)transform.position).normalized;

            animator.SetFloat("Horizontal", -direction.x);
            animator.SetFloat("Vertical", -direction.y);

            if (path == null)
                return;

            if (currentWaypoint >= path.vectorPath.Count)
                return;

            // Calcul d'où aller puis création de la force. On vérifie si c'est le didacticiel ou non puisque les ennemis
            // ne doivent pas de déplacer dans le didacticiel
            if (!isDidacticiel)
            {
                Vector2 whereGo = ((Vector2)path.vectorPath[currentWaypoint] - rb.position).normalized;
                Vector2 force = whereGo * speed * Time.deltaTime;

                if (IsThereAnObstacle())    // Si il y a un obstacle en le militaire et le joueur, le militaire essaie de le contourner
                {
                    rb.AddForce(force);
                } 
                else                        // Sinon, cela dépend de la distance en le joueur et le militaire
                {
                    // Si la distance est plus grande que la distance d'arrêt, alors il avance
                    if (Vector2.Distance(transform.position, playerPos.position) > stoppingDistance)    
                    {
                        rb.AddForce(force);
                    }
                    // Si la distance est plus faible de la distance d'arrêt, mais plus grande que la distance de retraite, alors il ne bouge plus
                    else if (Vector2.Distance(transform.position, playerPos.position) < stoppingDistance && Vector2.Distance(transform.position, playerPos.position) > retreatDistance)
                    {
                        transform.position = this.transform.position;
                        moving = false;
                    }
                    // Si la distance est plus faible de la distance de retraite, alors le militaire recule
                    else if (Vector2.Distance(transform.position, playerPos.position) < retreatDistance)
                    {
                        rb.AddForce(-force);
                    }
                }
            }

            if (isDidacticiel)  // Si c'est le didacticiel, le militaire ne bouge pas
                moving = false;

            animator.SetBool("Moving", moving);

            float distance = Vector2.Distance(rb.position, path.vectorPath[currentWaypoint]);

            if (distance < nextWaypointDistance)
            {
                currentWaypoint++;
            }

            // Timer du tir
            if (timeBtwShots <= 0 && !isDidacticiel)
            {
                if (Vector2.Distance(transform.position, playerPos.position) < shootingDistance)
                    Shot();
            }
            else
            {
                timeBtwShots -= Time.deltaTime;
            }
        }

        // Timer pour se faire taper
        if (timeBtwGettingHit != startTimeBtwGettingHit)
        {
            timeBtwGettingHit -= Time.deltaTime;
            if (timeBtwGettingHit <= 0)
                timeBtwGettingHit = startTimeBtwGettingHit;
        }

        // Timer du temps avant de mourir lorsqu'on atteint 0hp pour laisser le temps au son de se faire
        if (timeBeforeDying > 0)
        {
            timeBeforeDying -= Time.deltaTime;
            if (timeBeforeDying <= 0)
            {
                Destroy(gameObject);
            }
        }
    }

    public void Die()
    {
        if (isDidacticiel)                                                          // Si le militaire meurt pendant le didacticiel, cela influe l'humanité du joueur
        {
            GlobalVariables.humanity += 3;
        }

        Instantiate(deathEffect, transform.position, Quaternion.identity);          // On crée l'effet de particule

        if (player.forme == "Human")                                                // Si le joueur est encore sous forme humaine, alors il y a une chance de laisse tomber une batterie
        {
            int randPos = Random.Range(0, batteryDropRate);
            if (randPos == 0)
                Instantiate(battery, transform.position, Quaternion.identity);
        }

        spawner.DeathOfMilitary();
        timeBeforeDying = startTimeBeforeDying;
        dead = true;
    }

    public void GetHit(int damage)
    {
        if (timeBtwGettingHit == startTimeBtwGettingHit)
        {
            hitSound.Play();
            health -= damage;
            StartCoroutine("HitAnimation");
            if (health <= 0 && !dead)
            {
                Die();
            }
            timeBtwGettingHit -= Time.deltaTime;
        }
    }

    private IEnumerator HitAnimation()
    {
        spriteRenderer.color = Color.red;
        yield return new WaitForSeconds(startTimeBtwGettingHit);
        spriteRenderer.color = Color.white;
    }

    private void OnPathComplete(Path p)
    {
        if (!p.error)
        {
            path = p;
            currentWaypoint = 0;
        }
    }

    private void UpdatePath()
    {
        if (seeker.IsDone())        // On vérifie s'il est pas déjà en train de calculer un chemin
        {
            seeker.StartPath(rb.position, playerPos.position, OnPathComplete);
        }
    }

    private void Shot()
    {
        // Si il n'y a pas d'obstacle entre le joueur et le militaire, alors il tire
        if (!IsThereAnObstacle())
        {
            gunController.Shot();
            timeBtwShots = startTimeBtwShots;
        }
    }

    private bool IsThereAnObstacle()
    {
        bool aimed = false;         // true si il y a un mur

        hits = Physics2D.RaycastAll(transform.position, -direction, Vector2.Distance(transform.position, playerPos.position));
        //Debug.DrawRay(transform.position, -direction*100, Color.red);     // Pour afficher le raycast à des fins de debugging

        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i].collider.CompareTag("Obstacle") || hits[i].collider.CompareTag("Wall"))
            {
                i = hits.Length;
                aimed = true;
            }
        }

        return aimed;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player") && isDidacticiel)
        {
            Physics2D.IgnoreCollision(GameObject.FindGameObjectWithTag("Player").GetComponent<Collider2D>(), gameObject.GetComponent<Collider2D>());
        }
    }
}
