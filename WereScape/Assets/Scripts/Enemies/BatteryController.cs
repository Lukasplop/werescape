﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatteryController : MonoBehaviour
{
    public float lifetime = 10f;                // La duree de vie de la batterie, le temps avant qu'elle disparaisse

    private GameObject player;                  // Une référence sur le joueur pour remplir sa barre de lumière
    private AudioSource collectSound;           // Le bruit que fait la batterie quand elle est ramassée

    public float startTimeBeforeDisapear;       // Pour avoir le temps de jouer le son
    private float timeBeforeDisapear = 0;

    public bool isDidacticiel = false;          // Si c'est le didacticiel, la batterie ne dispawn pas pour laisser le temps au joueur

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        collectSound = GetComponentInChildren<AudioSource>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            collectSound.Play();
            player = GameObject.FindGameObjectWithTag("Player");
            player.GetComponent<PlayerController>().BatteryFull();
            timeBeforeDisapear = startTimeBeforeDisapear;
        }
    }

    private void Update()
    {
        lifetime -= Time.deltaTime;
        if (lifetime <= 0f)
        {
            Die();
        }

        if (timeBeforeDisapear > 0)
        {
            timeBeforeDisapear -= Time.deltaTime;
            if (timeBeforeDisapear <= 0)
            {
                Die();
            }
        }
    }

    public void Die()
    {
        if (!isDidacticiel)
            Destroy(gameObject);
    }
}
