﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Classe qui permet d'activer ou désactiver un GameObject, elle est utile lorsqu'on a une référence
// sur un objet qu'on veut parfois désactiver. Si on fait pas comme ça, on ne peut plus le réactivé lorsqu'il
// est désactivé.
public class ActivateDeactivate : MonoBehaviour
{
    public void Activate()
    {
        gameObject.SetActive(true);
    }

    public void Deactivate()
    {
        gameObject.SetActive(false);
    }
}
