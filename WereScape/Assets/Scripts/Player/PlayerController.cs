﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public float speed;                        // Variable modifiable dans unity pour la vitesse de déplacement
    public int health = 10;                    // Les points de vie du personnages
    public int healthMax = 10;
    public string forme;                       // Prend la valeur "Human", "YoungWolf", "Wolf" ou "ALPHA"

    private FieldOfView fieldOfView;           // La lampe utilisée sous forme humaine

    private Rigidbody2D rb;                    // Une référence sur le rigidbody2D du personnage pour pouvoir le déplacer
    public Camera cam;                         // Une référence sur la caméra de la forme

    private float mx;                          // L'input en x du joueur
    private float my;                          // L'input en y du joueur

    private Vector2 moveVelocity;              // Le Vector2 de déplacement correspondant à l'input de déplacement du joueur
    private Vector2 moveInput;                 // Le Vector2 correspondant à l'input de déplacement du joueur

    private float timeBtwHits;                 // La durée restante d'invincibilité
    public float startTimeBtwHits;             // Le temps d'invincibilité après des dégats 

    private Vector2 mouseWorldPosition;        // La position de la souris dans le monde
    private Vector2 direction;                 // La direction vers laquelle est la souris 

    private Text healthDisplay;                // Le Hud montrant le nombre de point de vie restant au personnage
    private HealthBar healthBar;

    private bool dashing = false;              // Pour ne pas bouger lorsque le perso dash

    public Animator animator;                  // L'animateur de notre objet

    public AudioSource hitSound;               // Référence sur le bruit que fait le joueur lorsqu'il se fait toucher
    private AudioSource music;                  // Référence sur la musique pour pouvoir fade 

    // Des couleurs utilisées pour les frames d'invincibilité et de hit
    private SpriteRenderer spriteRenderer;
    private Color red = Color.red;
    private Color white = Color.white;
    private Color semiTransparent = new Color(255, 255, 255, 127);
    private Color invisibile = new Color(255, 255, 255, 0);

    void Start()
    {
        // Initialisation du timer 
        timeBtwHits = startTimeBtwHits;

        // Récupération des références
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        music = GetComponent<AudioSource>();
        fieldOfView = GameObject.FindGameObjectWithTag("FieldOfView").GetComponent<FieldOfView>();
        healthDisplay = GameObject.FindGameObjectWithTag("HealthDisplay").GetComponent<Text>();
        healthBar = GameObject.FindGameObjectWithTag("HealthBar").GetComponent<HealthBar>();

        cam.backgroundColor = GameObject.FindGameObjectWithTag("Canvas").GetComponent<CanvasController>().rgbBackgroundColor;
        FadeFromZeroToZeroFive();
    }

    void Update()
    {
        // Récupération des inputs et calcul du déplacement approprié
        mx = Input.GetAxisRaw("Horizontal");
        my = Input.GetAxisRaw("Vertical");
        moveInput = new Vector2(mx, my);
        moveVelocity = moveInput.normalized * speed;

        // On convertit la position de la souris en coordonnées
        mouseWorldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        // On calcule la direction
        direction = (mouseWorldPosition - (Vector2)transform.position).normalized;

        // Si le joueur est encore humain, on initialise la lampe torche
        if (forme == "Human")
        {
            fieldOfView.SetAimDirection(direction);
            fieldOfView.SetOrigin(transform.position);
        }

        animator.SetFloat("Horizontal", direction.x);
        animator.SetFloat("Vertical", direction.y);
        animator.SetFloat("Magnitude", moveInput.magnitude);

        // Mise à jour de la frame d'invincibilité
        if (timeBtwHits < startTimeBtwHits)
        {
            timeBtwHits -= Time.deltaTime;
            if (timeBtwHits <= 0)
            {
                timeBtwHits = startTimeBtwHits;
            }
        }

        // Mise à jour de la barre de vie
        healthDisplay.text = health.ToString() +" / " + healthMax.ToString() ;
        healthBar.SetSize((float)health / healthMax);
        if (0.75 <= (float)health/healthMax && (float)health / healthMax <= 1)
        {
            healthBar.SetColor(Color.green);
        }
        else if (0.5 <= (float)health / healthMax && (float)health / healthMax <= 0.75)
        {
            healthBar.SetColor(Color.yellow);
        }
        if (0.25 <= (float)health / healthMax && (float)health / healthMax <= 0.5)
        {
            healthBar.SetColor(Color.Lerp(Color.red, Color.yellow, 0.5f));
        }
        if (0 <= (float)health / healthMax && (float)health / healthMax <= 0.25)
        {
            healthBar.SetColor(Color.red);
        }
    }

    private void FixedUpdate()
    {
        // Si le joueur n'est pas en train de dash, alors il se déplace
        if (!dashing)
            rb.MovePosition(rb.position + moveVelocity * Time.fixedDeltaTime);
    }

    public void GetHit(int damage)
    {
        if (timeBtwHits == startTimeBtwHits && dashing == false)
        {
            if (GlobalVariables.forme != GlobalVariables.Formes.ALPHA)
            {
                hitSound.Play();
                StartCoroutine("Flasher");
                health -= damage;
            }
            if (health == 0)
            {
                Die();
            }
            timeBtwHits -= Time.deltaTime;
            
        }
    }

    public void Die()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        //Destroy(gameObject);
    }

    public void StopMovingWhileDashing()
    {
        dashing = true;
    }

    public void StartMovingAgain()
    {
        dashing = false;
    }

    private IEnumerator Flasher()
    {
        // Pour montrer qu'on s'est fait toucher
        spriteRenderer.color = red;
        yield return new WaitForSeconds(startTimeBtwHits / 5);

        // Pour clignoter pendant la frame d'invicibilité
        spriteRenderer.color = semiTransparent;
        yield return new WaitForSeconds(startTimeBtwHits / 10);
        spriteRenderer.color = invisibile;
        yield return new WaitForSeconds(startTimeBtwHits / 10);
        spriteRenderer.color = semiTransparent;
        yield return new WaitForSeconds(startTimeBtwHits / 10);
        spriteRenderer.color = invisibile;
        yield return new WaitForSeconds(startTimeBtwHits / 10);
        spriteRenderer.color = semiTransparent;
        yield return new WaitForSeconds(startTimeBtwHits / 10);
        spriteRenderer.color = invisibile;
        yield return new WaitForSeconds(startTimeBtwHits / 10);
        spriteRenderer.color = semiTransparent;
        yield return new WaitForSeconds(startTimeBtwHits / 10);
        spriteRenderer.color = white;
    }

    public void BatteryFull()
    {
        gameObject.GetComponent<BatteryBarController>().BatteryFull();
    }

    public void Activate()
    {
        gameObject.SetActive(true);
    }

    public void Deactivate()
    {
        gameObject.SetActive(false);
    }

    public void SetForme(string form)
    {
        forme = form;
    }

    public FieldOfView GetFieldOfView()
    {
        return fieldOfView;
    }

    public void FadeToZero()
    {
        StartCoroutine(FadeAudioSource.StartFade(music, 1f, 0f));
    }
   
    public void FadeFromZeroToZeroFive()
    {
        StartCoroutine(FadeAudioSource.StartFade(music, 1f, 0.5f));
    }
}
    
    

