﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class DashController : MonoBehaviour
{

    private PlayerController playerController;          // Une référence sur l'autre script pour bloquer les déplacements lors d'un dash
    public int dashSpeed;                               // La vitesse du dash
    public int dashDamage;                              // Les dégats du dash
    private Rigidbody2D rb;
    private float timeBtwDash;                          // La durée restante avant de pouvoir dash
    public float startTimeBtwDash;                      // Le cooldown pour dash
    private Vector2 dashPosition;                       // La position vers laquelle dash
    private Vector2 dashDirection;
    private bool dashing = false;                       // Le joueur est-il en plein dash
    private RaycastHit2D[] hits;                        // Pour détecter si y'a un mur ou un obstacle juste là où on dash

    private string[] next_to_wall = new string[] { "none", "none" };      // Pour vérifier s'il est prêt de murs pour éviter de clipper dedans en dashant

    private ActivateDeactiveClaws claws;                // Pour pouvoir empêcher de taper pendant un dash
    public AudioSource dashSound;                       // Référence sur le son du dash
    private Image progressBar;                          // Avancement du cooldown


    private void Start()
    {
        claws = transform.parent.GetComponentInChildren<ActivateDeactiveClaws>();
        playerController = gameObject.GetComponent<PlayerController>();
        rb = GetComponent<Rigidbody2D>();
        progressBar = GameObject.FindGameObjectWithTag("ProgressBarDash" + playerController.forme).GetComponent<Image>();

        timeBtwDash = startTimeBtwDash;
    }

    void Update()
    {
        // Si on appuie sur la touche de dash et que la compétence n'est pas en cooldown
        if ((Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space)) && timeBtwDash == startTimeBtwDash && GlobalVariables.isPaused == false)
        {
            Dash();
        }

        // Avancement du timer cooldown si la compétence est en cooldown
        if (timeBtwDash < startTimeBtwDash)
        {
            timeBtwDash -= Time.deltaTime;
            progressBar.fillAmount = timeBtwDash / startTimeBtwDash;

            if (timeBtwDash <= 0)
            {
                timeBtwDash = startTimeBtwDash;
            }
        }

        // Fin du dash lorsqu'on atteint la destination
        if (Vector2.Distance(rb.position, dashPosition) < 0.1 && dashing == true)
            StopDashing();
    }

    private void FixedUpdate()
    {
        // Si on est en train de dash, on modifie la position
        if (dashing)
        {
            rb.position = Vector2.MoveTowards(transform.position, dashPosition, dashSpeed * Time.deltaTime);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Si on rencontre un mur, on arrête de dash
        if (collision.CompareTag("Wall") || collision.CompareTag("Obstacle"))
        {
            addNext_To_Wall(collision.name);
            StopDashing();
        }

        // Si on rencontre un ennemi, on lui inflige des dégats si on dash
        if (collision.CompareTag("Scientist") && dashing == true)
        {
            collision.GetComponent<ScientistController>().GetHit(dashDamage);
        }
        if (collision.CompareTag("Military") && dashing == true)
        {
            collision.GetComponent<MilitaryController>().GetHit(dashDamage);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Wall") || collision.CompareTag("Obstacle"))
        {
            removeNext_To_Wall(collision.name);
        }
    }

    private void Dash()
    {
        // On vérifie qu'on ne va pas dash dans un mur 
        bool go_continue = true;
        dashPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        dashDirection = (dashPosition - rb.position).normalized;

        hits = Physics2D.LinecastAll(rb.position, dashPosition+dashDirection*10);
        //Debug.DrawLine(rb.position, dashPosition+dashDirection*10, Color.red);

        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i].collider.CompareTag("Obstacle") || hits[i].collider.CompareTag("Wall"))
            {
                if ((hits[i].distance < 0.2 && containsNext_To_Wall("none")) || (hits[i].distance < 10 && containsNext_To_Wall(hits[i].collider.name)))
                {
                    i = hits.Length;
                    go_continue = false;
                }
            }
        }

        // Si il n'y a pas de mur trop proche, on commence à dash
        if(go_continue)
        {
            dashSound.Play();
            playerController.StopMovingWhileDashing();
            dashing = true;
            timeBtwDash -= Time.deltaTime;
            claws.Deactivate();
        }
    }

    private void StopDashing()
    {
        playerController.StartMovingAgain();
        dashing = false;
        claws.Activate();
    }

    private void addNext_To_Wall(string wall)
    {
        if (next_to_wall[0] == "none")
            next_to_wall[0] = wall;
        else if (next_to_wall[1] == "none")
            next_to_wall[1] = wall;
    }

    private void removeNext_To_Wall(string wall)
    {
        if (next_to_wall[0] == wall)
            next_to_wall[0] = "none";
        else if (next_to_wall[1] == wall)
            next_to_wall[1] = "none";
    }

    private bool containsNext_To_Wall(string wall)
    {
        if (next_to_wall[0] == wall)
            return true;
        else if (next_to_wall[1] == wall)
            return true;
        else
            return false;
    }
}
