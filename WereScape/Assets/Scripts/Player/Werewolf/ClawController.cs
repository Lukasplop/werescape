﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ClawController : MonoBehaviour
{
    public CreatingMelee claw;                         // Une référence sur le script de la griffe pour pouvoir le faire apparaître et disparaître
    public bool leftClaw;                              // Booléen pour savoir quelle griffe c'est

    private Vector2 mouseWorldPosition;                // La position de la souris dans le monde
    private Vector2 direction;                         // La direction vers laquelle est la souris en partant du pivot

    private float activeClawTime;                      // La durée de visibilité de la griffe, reprend la valeur de startActiveClawTime à chaque itération
    public float startActiveClawTime;                  // La durée de visibilité de la griffe

    private float cooldownClawTime;                    // La valeur actuelle du cooldown de la griffe
    public float startCooldownClawTime;                // Le cooldown de la griffe

    private float currentRotation = 0f;                // Rotation actuelle

    private Image progressBar;                         // Avancement du rechargement de la compétence


    void Start()
    {
        activeClawTime = startActiveClawTime;
        cooldownClawTime = startCooldownClawTime;

        claw = gameObject.GetComponentInChildren<CreatingMelee>();

        claw.Deactivate();                             // On désactive la griffe ici. Si on le désactive avant, on ne peut pas récupérer la référence
        
    }


    void Update()
    {
        // Si on est pas en cooldown
        if (activeClawTime == startActiveClawTime)
        {
            // On convertit la position de la souris en coordonnées
            mouseWorldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            // On calcule la direction
            direction = -(mouseWorldPosition - (Vector2)transform.position).normalized;
            // et on modifie directement le transform
            transform.up = direction;

            // Si on appuie sur la touche d'attaque
            if (Input.GetMouseButtonDown(1) && cooldownClawTime == startCooldownClawTime)
            {
                ActivateClawHit();
            }
        }
        // Sinon si on est on train d'attaquer
        else if (activeClawTime < startActiveClawTime)
        {
            // On met à jour la rotation
            activeClawTime -= Time.deltaTime;
            currentRotation = transform.eulerAngles.z;

            if (leftClaw)
            {
                transform.rotation = Quaternion.Euler(0, 0, currentRotation + (90 * Time.deltaTime) / startActiveClawTime);
            }
            else
            {
                transform.rotation = Quaternion.Euler(0, 0, currentRotation - (90 * Time.deltaTime) / startActiveClawTime);
            }
               
            if (activeClawTime <= 0)
            {
                activeClawTime = startActiveClawTime;
                claw.Deactivate();
            }
        }

        // Avancement du cooldown si l'attaque est en cooldown
        if (cooldownClawTime < startCooldownClawTime)
        {
            cooldownClawTime -= Time.deltaTime;
            if (leftClaw)
                progressBar.fillAmount = cooldownClawTime / startCooldownClawTime;
            if (cooldownClawTime <= 0)
            {
                cooldownClawTime = startCooldownClawTime;
            }
            
        }
    }

    private void ActivateClawHit()
    {
        claw.Activate();                            // On active les griffes
        currentRotation = transform.eulerAngles.z;  // On initialise la rotation

        // Mise à jour de la rotation
        if (leftClaw)
            transform.rotation = Quaternion.Euler(0, 0, currentRotation - 90);
        else
            transform.rotation = Quaternion.Euler(0, 0, currentRotation + 90);

        // On active les timers
        activeClawTime -= Time.deltaTime;
        cooldownClawTime -= Time.deltaTime;
    }

    public void ActivateCooldownClaws()
    {
        progressBar = GameObject.FindGameObjectWithTag("ProgressBarClaws").GetComponent<Image>();
    }

}
