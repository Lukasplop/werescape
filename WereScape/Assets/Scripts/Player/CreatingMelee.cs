﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatingMelee : MonoBehaviour
{
    public int damage;              // Les dégats de l'arme de mêlée

    private AudioSource clawSound;  // Référence sur le bruit de l'arme

    private void Start()
    {
        clawSound = GetComponentInChildren<AudioSource>();
    }
    public void Activate()
    {
        gameObject.SetActive(true);
        if (clawSound != null)
            clawSound.Play();
    }

    public void Deactivate()
    {
        gameObject.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Scientist"))
        {
            collision.GetComponent<ScientistController>().GetHit(damage);
        }
        if (collision.CompareTag("Military"))
        {
            collision.GetComponent<MilitaryController>().GetHit(damage);
        }
    }
}
