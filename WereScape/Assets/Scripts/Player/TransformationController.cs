﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformationController : MonoBehaviour
{
    private GameObject[] forms;         // Toutes les formes possibles du joueur
    private GameObject human;           // Référence sur la forme humaine
    private GameObject youngWolf;       // Référence sur la forme de jeune loup
    private GameObject wolf;            // Référence sur la forme de loup
    private GameObject alpha;           // Référence sur la forme d'alpha
    private GameObject canvas;          // Référence sur le canvas
    private GameObject spawner;         // Référence sur le spawner

    private int humanity;               // humanité du joueur
    private int humanityYoungWolf;      // Seuil d'humanité du jeune loup
    private int humanityWolf;           // Seuil d'humanité du loup
    private int humanityAlpha;          // Seuil d'humanité de l'alpha

    private void Start()
    {
        // Récupération de l'humanité et des seuils
        humanity = GlobalVariables.humanity;
        humanityYoungWolf = GlobalVariables.evolutionYoungWolf;
        humanityWolf = GlobalVariables.evolutionWereWolf;
        humanityAlpha = GlobalVariables.evolutionALPHA;

        // Récupération des références
        canvas = GameObject.FindGameObjectWithTag("Canvas");
        forms = GameObject.FindGameObjectsWithTag("Player");
        spawner = GameObject.FindGameObjectWithTag("Spawner");

        for (int i = 0; i < forms.Length; i++)
        {
            // Récupération des formes 
            if(forms[i].name == "Human")
                human = forms[i];
            else if (forms[i].name == "YoungWolf")
                youngWolf = forms[i];
            else if (forms[i].name == "Wolf")
                wolf = forms[i];
            else if (forms[i].name == "ALPHA")
                alpha = forms[i];
        }
        
        // Transformation à la bonne forme en fonction de l'humanité actuelle et des seuils
        if (humanity < humanityYoungWolf)
            Transformation(human);
        else if (humanity < humanityWolf)
            Transformation(youngWolf);
        else if (humanity < humanityAlpha)
            Transformation(wolf);
        else
            Transformation(alpha);
    }

    // Fonction transformant le joueur
    private void Transformation(GameObject form)
    {
        for (int i = 0; i < forms.Length; i++)
        {
            if (form != forms[i])   // Désactive toutes les formes sauf la forme active
                forms[i].GetComponent<PlayerController>().Deactivate();
            else if (form == forms[i])
            {
                // Modification de la difficulté et du canvas en fonction de la forme actuelle
                forms[i].GetComponent<PlayerController>().SetForme(form.name);
                canvas.GetComponent<CanvasController>().ModifyCanvas();
                spawner.GetComponent<SpawnerController>().ModifyDifficulty();
                if (form.name != "Human")
                {
                    forms[i].GetComponentInChildren<ClawController>().ActivateCooldownClaws();
                }
            }
        }
    }
}
