﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatteryBar : MonoBehaviour
{
    private Transform bar;              // Référence sur la position de la barre
    private SpriteRenderer barSprite;   // Référence sur le SpriteRenderer

    private void Awake()
    {
        bar = transform.Find("Bar");
        barSprite = bar.Find("BarSpriteLight").GetComponent<SpriteRenderer>();
    }

    public void SetSize(float sizeNormalized)
    {
        bar.localScale = new Vector3(sizeNormalized, 1f);
    }

    public void SetColor(Color color)
    {
        barSprite.color = color;
    }

}
