﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KnifeController : MonoBehaviour
{
    public CreatingMelee knife;                         // Une référence sur le script du couteau pour pouvoir le faire apparaître et disparaître
    public GunController gun;                           // Une référence sur le script du gun pour pouvoir le faire apparaître et disparaître

    private Vector2 mouseWorldPosition;                 // La position de la souris dans le monde
    private Vector2 direction;                          // La direction vers laquelle est la souris en partant du pivot

    private AudioSource slashSound;                     // Référence sur le son du couteau

    private float activeKnifeTime;                      // La durée de visibilité du couteau, reprend la valeur de startActiveKnifeTime à chaque itération
    public float startActiveKnifeTime;                  // La durée de visibilité du couteau
    private Image progressBar;

    private float currentRotation = 0f;                 // Rotation actuelle du couteau

    void Start()
    {
        // Initialisation du timer
        activeKnifeTime = startActiveKnifeTime;

        // Récupération des références
        slashSound = GetComponentInChildren<AudioSource>();
        knife = gameObject.GetComponentInChildren<CreatingMelee>();
        gun = transform.parent.GetComponentInChildren<GunController>();

        knife.Deactivate();                             // On désactive le couteau ici. Si on le désactive avant, on ne peut pas récupérer la référence
        progressBar = GameObject.FindGameObjectWithTag("ProgressBarKnife").GetComponent<Image>();
    }


    void Update()
    {
        if (activeKnifeTime == startActiveKnifeTime)
        {
            // On convertit la position de la souris en coordonnées
            mouseWorldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            // On calcule la direction
            direction = -(mouseWorldPosition - (Vector2)transform.position).normalized;
            // et on modifie directement le transform
            transform.up = direction;

            // Si on a le cooldown et qu'on appuie sur le clic du coup de couteau
            if (Input.GetMouseButtonDown(1))
            {
                slashSound.Play();
                ActivateKnifeHit();
            }
        }
        else if (activeKnifeTime < startActiveKnifeTime)        // Si on est en plein coup de couteau
        {
            activeKnifeTime -= Time.deltaTime;
            currentRotation = transform.eulerAngles.z;
            transform.rotation = Quaternion.Euler(0, 0, currentRotation - (180 * Time.deltaTime) / startActiveKnifeTime); // On met à jour la rotation
            progressBar.fillAmount = activeKnifeTime / startActiveKnifeTime;
            if (activeKnifeTime <= 0)
            {
                activeKnifeTime = startActiveKnifeTime;
                knife.Deactivate();
                gun.Activate();
            }
        }
    }

    private void ActivateKnifeHit()
    {
        gun.Deactivate();                                                   // Le pistolet est désactivé
        knife.Activate();                                                   // Le couteau est activé
        currentRotation = transform.eulerAngles.z;                          // On met à jour la rotation initiale
        transform.rotation = Quaternion.Euler(0, 0, currentRotation+90);    
        activeKnifeTime -= Time.deltaTime;
    }
}
