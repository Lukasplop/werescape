﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatteryBarController : MonoBehaviour
{
    private BatteryBar batteryBar;             // Référence sur la barre de batterie

    public float startLightTime;               // Durée de la lampe
    private float leftLightTime;               // Durée restante de la lampe 

    private FieldOfView fieldOfView;           // Référence sur le fieldOfView

    void Start()
    {
        batteryBar = GameObject.FindGameObjectWithTag("BatteryBar").GetComponent<BatteryBar>();
        fieldOfView = GameObject.FindGameObjectWithTag("FieldOfView").GetComponent<FieldOfView>();

        leftLightTime = startLightTime;
    }

    void Update()
    {
        // Si le temps restant de lumière est supérieur à 0, alors on le décrémente et on met à jour l'affichage
        if (leftLightTime <= startLightTime && leftLightTime > 0)
        {
            leftLightTime -= Time.deltaTime;

            batteryBar.SetSize(leftLightTime / startLightTime);
            if (leftLightTime <= startLightTime * 0.25 && leftLightTime > 0)    // Si le temps restant est < 25%, alors la distance d'éclairage commence à diminuer
            {
                fieldOfView.SetDistance((float)((leftLightTime / (0.25 * startLightTime)) * fieldOfView.GetViewDistanceMax()));
            }
            else if (leftLightTime <= 0)                                        // Si le temps restant est <= 0, alors la lampe s'eteind
            {
                fieldOfView.TurnOffLight();
            }
        }
    }

    public void BatteryFull()
    {
        leftLightTime = startLightTime;
        fieldOfView.TurnOnLight();
    }
}
