﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GunController : MonoBehaviour
{
    public GameObject shot;                 // Reference sur l'objet projectile qu'on va envoyer
    public GameObject firingPoint;          // La position de depart du projectile (le bout du pistolet)
    public GameObject gunPivot;             // Référence sur le gunPivot

    private AudioSource gunshot;            // Référence sur le bruit du pistolet 

    public int nbAmmo;                      // Le nombre de balle dans le chargeur
    private int nbAmmoLeft;                 // Le nombre de balle qu'il reste dans le chargeur

    public float startReloadingTime;        // Durée de rechargement
    private float reloadingTime;            // Durée restante de rechargement 

    private Image progressBar;              // L'image correspondant à la progression du rechargement


    private Text bulletDisplay;             // Le Hud montrant le nombre de balle restant au personnage

    private void Start()
    {
        // Si le chargeur est amélioré, le joueur dispose de 2 fois plus de balle dans son chargeur
        if (GlobalVariables.chargeurUpgrade)
        {
            nbAmmo *= 2;
        }
        nbAmmoLeft = nbAmmo;

        // Initialisation du timer
        reloadingTime = startReloadingTime;

        // On récupère les références
        gunshot = GetComponentInChildren<AudioSource>();
        bulletDisplay = GameObject.FindGameObjectWithTag("BulletDisplay").GetComponent<Text>();
        progressBar = GameObject.FindGameObjectWithTag("ProgressBar").GetComponent<Image>();
    }

    void Update()
    {
        bulletDisplay.text = nbAmmoLeft.ToString();     // On affiche le nombre de balle restant dans le chargeur

        // Si le pistolet est en train de se recharger, on décrémente la durée
        if (reloadingTime < startReloadingTime)
        {
            reloadingTime -= Time.deltaTime;
            progressBar.fillAmount = reloadingTime;
            if (reloadingTime <= 0)
            {
                Reload();
            }
        }

        // Si on appuie sur la touche R, alors on recharge
        if (Input.GetKeyDown(KeyCode.R) && nbAmmoLeft != 0 && nbAmmo != nbAmmoLeft)
        {
            reloadingTime -= Time.deltaTime;
        }

        // Si on tire et qu'on est pas en train de recharger, alors on tire vraiment
        if (Input.GetMouseButtonDown(0) && reloadingTime == startReloadingTime)
        {
            Instantiate(shot, firingPoint.transform.position, gunPivot.transform.rotation);
            nbAmmoLeft -= 1;
            gunshot.Play();
            if(nbAmmoLeft == 0)
            {
                reloadingTime -= Time.deltaTime;
            }
        }
    }

    public void Activate()
    {
        gameObject.SetActive(true);
    }

    public void Deactivate()
    {
        gameObject.SetActive(false);
    }

    public void Reload()
    {
        nbAmmoLeft = nbAmmo;
        reloadingTime = startReloadingTime;
    }
}
