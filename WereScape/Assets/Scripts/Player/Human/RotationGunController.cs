﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationGunController : MonoBehaviour
{
    private Vector2 mouseWorldPosition;     // La position de la souris dans le monde
    private Vector2 direction;              // La direction vers laquelle est la souris en partant du pistolet

    public float rightSideXPos;             // La position en x du pivot de droite
    public float leftSideXPos;              // la position en x du pivot de gauche
    public float yPos;                      // la position en y des pivots

    public GameObject gun;                  // Référence sur l'objet pistolet
    public Transform playerPos;             // Référence sur la position du joueur

    private Vector2 rightPos;               // La position de la main droite du joueur
    private Vector2 leftPos;                // La position de la main gauche du joueur

    private bool onRightSide = true;        // Le side actuel du pistolet            

    private void Start()
    {
        rightPos = new Vector2(rightSideXPos, yPos);
        leftPos = new Vector2(leftSideXPos, yPos);

        playerPos = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void FixedUpdate()
    {
        // On convertit la position de la souris en coordonnées
        mouseWorldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        // On calcule la direction
        direction = -(mouseWorldPosition - (Vector2)playerPos.position).normalized;
        
        if (direction.x < 0 && !onRightSide)
        {
            GoRightSide();
        }
        else if (direction.x >= 0 && onRightSide)
        {
            GoLeftSide();
        }

        // et on modifie directement le transform
        transform.up = direction;
    }

    private void GoLeftSide()
    {
        transform.position = leftPos + (Vector2)playerPos.position;
        RotateGun();
        onRightSide = false;
    }

    private void GoRightSide()
    {
        transform.position = rightPos + (Vector2)playerPos.position;
        RotateGun();
        onRightSide = true;
    }

    private void RotateGun()
    {
        var rot = gun.transform.rotation;
        gun.transform.rotation = rot * Quaternion.Euler(0, 180, 0);

        gun.transform.localPosition = new Vector2(-gun.transform.localPosition.x, gun.transform.localPosition.y);
    }
}
