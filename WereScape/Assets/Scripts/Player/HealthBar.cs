﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour
{
    private Transform bar;              // Référence sur la barre
    private SpriteRenderer barSprite;   // Référence sur le SpriteRenderer


    private void Awake()
    {
        bar = transform.Find("Bar");
        barSprite = GameObject.Find("BarSpriteHealth").GetComponent<SpriteRenderer>();
    }

    public void SetSize(float sizeNormalized)
    {
        bar.localScale = new Vector3(sizeNormalized, 1f);
    }
    public void SetColor(Color color)
    {
        barSprite.color = color;
    }

}
