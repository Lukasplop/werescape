﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIWriterFin : MonoBehaviour
{
    private Text StoryText;                               // Variable contenant sur le texte du niveau

    private Image Background;

    private TextWriter.TextWriterSingle textWriterSingle;

    public TextWriter writer;                            // Référence sur le text writer pour la musique

    // Références sur les 3 boutons de choix
    public GameObject Choice01;
    public GameObject Choice02;
    public GameObject Choice03;

    public Sprite LeSecret; // OK

    public Sprite LaTraque; // OK

    public Sprite DuSangSurLesMains;

    public Sprite LoupSolitaire; // OK

    public Sprite LaMeute; // OK

    public Sprite LALPHA_Armageddon; // OK

    private int state;

    private void Awake()
    {
        StoryText = transform.Find("Canvas").Find("TransparentPanel").Find("StoryText").GetComponent<Text>();
        Background = transform.Find("Canvas").Find("Background").GetComponent<Image>();
    }

    private void HideChoices()
    {
        Choice01.SetActive(false);
        Choice02.SetActive(false);
        Choice03.SetActive(false);
    }

    private void ShowChoices()
    {
        Choice01.SetActive(true);
        Choice02.SetActive(true);
    }

    private void ShowChoice()
    {
        Choice03.SetActive(true);
    }

    private void Start()
    {
        if (GlobalVariables.forme == GlobalVariables.Formes.Human && GlobalStory.foundDetonateur && GlobalStory.killPrisoner)
        {
            Choice01.GetComponentInChildren<Text>().text = "Détruire";
            Choice02.GetComponentInChildren<Text>().text = "Ne rien faire";
            string message = GlobalStory.storyFinH_start_kill_deto;
            textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoices);
            state = 0;
        }
        else
        {
            state = 1;
            Choice3();
        }
    }

    public void Choice1()
    {
        if (state == 0)
        {
            if (GlobalVariables.forme == GlobalVariables.Formes.Human)
            {
                GlobalStory.explodeLaboratory = true;
                ++state;
                Choice3();
            }
        }
    }

    public void Choice2()
    {
        if (state == 0)
        {
            ++state;
            Choice3();
        }
    }

    public void Choice3()
    {
        if (state == 1)
        {
            if (GlobalVariables.forme == GlobalVariables.Formes.Human)
            {
                if (!GlobalStory.killPrisoner)
                {
                    Background.sprite = DuSangSurLesMains;
                    HideChoices();
                    Choice03.GetComponentInChildren<Text>().text = "FIN";
                    string message = GlobalStory.storyFin_DuSangSurLesMains;
                    textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoice);
                    state = 2;
                }
                else
                {
                    if (GlobalStory.explodeLaboratory)
                    {
                        Background.sprite = LeSecret;
                        HideChoices();
                        Choice03.GetComponentInChildren<Text>().text = "FIN";
                        string message = GlobalStory.storyFin_LeSecret;
                        textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoice);
                        state = 2;
                    }
                    else if (!GlobalStory.explodeLaboratory)
                    {
                        Background.sprite = LaTraque;
                        HideChoices();
                        Choice03.GetComponentInChildren<Text>().text = "FIN";
                        string message = GlobalStory.storyFin_LaTraque;
                        textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoice);
                        state = 2;
                    }
                }
            }
            else if (GlobalVariables.forme == GlobalVariables.Formes.YoungWolf)
            {
                if (GlobalStory.killPrisoner)
                {
                    Background.sprite = LaTraque;
                    HideChoices();
                    Choice03.GetComponentInChildren<Text>().text = "FIN";
                    string message = GlobalStory.storyFin_LaTraque_2;
                    textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoice);
                    state = 2;
                }
                else
                {
                    Background.sprite = DuSangSurLesMains;
                    HideChoices();
                    Choice03.GetComponentInChildren<Text>().text = "FIN";
                    string message = GlobalStory.storyFin_DuSangSurLesMains;
                    textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoice);
                    state = 2;
                }
            }
            else if (GlobalVariables.forme == GlobalVariables.Formes.WereWolf)
            {
                if (GlobalStory.killPrisoner)
                {
                    Background.sprite = LoupSolitaire;
                    HideChoices();
                    Choice03.GetComponentInChildren<Text>().text = "FIN";
                    string message = GlobalStory.storyFin_LoupSolitaire;
                    textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoice);
                    state = 2;
                }
                else
                {
                    Background.sprite = LaMeute;
                    HideChoices();
                    Choice03.GetComponentInChildren<Text>().text = "FIN";
                    string message = GlobalStory.storyFin_LaMeute;
                    textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoice);
                    state = 2;
                }
            }
            else
            {
                if (GlobalStory.killPrisoner)
                {
                    Background.sprite = LALPHA_Armageddon;
                    HideChoices();
                    Choice03.GetComponentInChildren<Text>().text = "FIN";
                    string message = GlobalStory.storyFin_LAlpha;
                    textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoice);
                    state = 2;
                }
                else
                {
                    Background.sprite = LALPHA_Armageddon;
                    HideChoices();
                    Choice03.GetComponentInChildren<Text>().text = "FIN";
                    string message = GlobalStory.storyFin_LArmageddon;
                    textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoice);
                    state = 2;
                }
            }
        }
        else
        {
            LoadNextScene(); 
        }
    }

    public void LoadNextScene()
    {
        writer.FadeToZero();
        GlobalVariables.forme = GlobalVariables.Formes.Human;
        GlobalVariables.humanity = 0;
        GlobalVariables.chargeurUpgrade = false;
        SceneManager.LoadScene("MenuPrincipal");
    }
}
