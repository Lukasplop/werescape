﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Classe créée pour récupérer des valeurs pour la partie narrative, apparemment plus efficace que DontDestroyOnLoad()
public class GlobalStory
{
    //  ###############    ACTIONS    #########################################################################################################
    static public bool freePrisoner      = false;         // Evènement : Sauver le prisonnier          (Scene 1 2)
    static public bool foundDetonateur   = false;         // Evènement : Trouver le détonateur         (Scene 3 4)
    static public bool killPrisoner      = false;         // Evènement : Tuer le prisonnier            (Scene 5 6)
    static public bool prisonerDead      = false;         // Evènement : Tuer le prisonnier            (Scene 5 6)
    static public bool explodeLaboratory = false;         // Evènement : Faire exploser le laboratoire (Scene Fin)

    //  ###############    TRANSFORMATIONS    #########################################################################################################
    // Texte de transformation en jeune loup
    static public string transfo_young = @"Au moment de franchir la porte, vous sentez une douleur au fond de vous. 

Recroquevillé sur vous même, vous sentez tout votre corps vous faire mal. Comme si il se transformait. Vous sentez vos pieds, vos bras et votre torse grandir. La douleur vous fait crier. Vous regardez vos mains, apeuré, ne comprenant pas ce qu’il se passe. Vos mains prennent l’aspect de pattes, vos ongles se transforment en griffes et vos oreilles s’allongent. 

Après de longues secondes à souffrir, la douleur s’arrête. Vous restez quelque temps accroupis essayant de reprendre votre souffle et votre calme, constatant que votre corps avait changé. Vous levez la tête et remarquez que votre vision s’est améliorée. Vous comprenez maintenant que les expériences qui ont été faites sur vous vous ont changés. Vous pouvez maintenant vous transformer en loup-garou. Mais la transformation n’est pas allée jusqu’au bout et vous le sentez. 

Peut-être qu’en laissant place à votre bestialité, vous vous transformerez complètement. Mais est-ce le bon choix ? Pas le temps d’y réfléchir, les ennemis sont proches, vous vous relevez, prêt à ouvrir la porte, laissant au sol vos armes maintenant inutiles.
";

    // Texte de transformation en loup garou
    static public string transfo_wolf = @"Encore une fois, avant de franchir la porte, vous sentez que votre corps se transforme. Vos pattes et vos griffes grandissent, votre corps devient plus grand et plus imposant. Votre tête se transforme et devient celle d’un loup-garou. Votre aspect humain disparaît après quelques secondes douloureuses. 

Cette fois-ci la transformation est totale. Vous n’avez d’humain plus que votre esprit. Mais pour combien de temps ? Vous sentez toutefois être plus puissant et plus résistant. 

Êtes-vous seulement arrêtable maintenant ? Que se passera-t-il si vous laissez encore votre animosité prendre place ? Pourrez-vous redevenir humain un jour ? Trop de questions qui resteront sans réponse pour l’instant. Vos sens fraîchement affûtés vous indiquent que de nouveaux ennemis approchent.
";

    // Texte de transformation en loup ALPHA
    static public string transfo_alpha = @"Au moment d’ouvrir la porte, vous sentez que votre tête vous fait mal. Votre corps se transforme encore. Vous devenez plus grand et plus imposant, vos griffes continuent de grandir, vos sens deviennent encore plus affûtés. 

Cette fois-ci la transformation ne semble pas être que physique… Votre esprit se trouble, peu à peu vous n’arrivez plus à réfléchir. Les derniers signes de votre humanité semblent disparaître. Vos cris se transforment en hurlement, vos pupilles deviennent de plus en plus rouges. Cette transformation semble être la dernière. 

Plus aucun retour n’est possible maintenant. Après quelques secondes, votre humanité a complètement disparu pour laisser place à une bestialité violente. Vous êtes devenu une bête sans raison assoiffée de sang. Sentant de nouvelles proies, vous poussez un hurlement avant de défoncer la porte et de vous diriger vers elles. Plus rien ne peut vous arrêter...
";



    //  ###############    INTRODUCTION    #########################################################################################################
    // Texte au démarrage de l'introduction
    static public string intro_start = @"Emprisonné dans un laboratoire secret perdu sur une île bien loin de toute civilisation depuis longtemps, vous vous réveillez dans votre cellule. Encore un doux rêve qui disparaît… 

“J’en ai marre. J'en ai marre des expériences, de cette cellule. Je veux sortir.. Je veux SORTIR.. Je VAIS sortir !” 

En disant ces mots, une puissance vous inonde. Oui.. C’est ce soir que vous allez sortir. Cette puissance n’est pas anodine car ce soir… c’est la pleine lune. C’est peut-être votre seule chance de sortir et vous le savez. Vous entendez des bruits de pas s’approcher de votre cellule. Un scientifique s’approche de la porte.

“Ouverture cellule 404 !” 

La porte blindée de votre cellule s’ouvre. Le scientifique, accompagné d’un garde armé, entre. 

“Numéro 42, tourne-toi, on y retourne”

Vous vous tournez, en souriant. C’est bien le signe que ce soir… est le soir où vous sortirez. Le scientifique s’approche de vous avec une seringue. Mais avant qu’il ait pu vous piquer, vous vous retournez et assommez le scientifique tout en lui volant sa seringue. Le militaire sort son arme et commence à viser mais trop tard. La seringue se vide et le militaire tombe, endormi. L’alarme sonne, les lumières s'éteignent et vous entendez au loin que votre action n'est pas passée inaperçue. Les militaires et scientifiques du laboratoire arrivent... Vous prenez les armes du militaire ainsi que sa lampe torche, prêt à en découdre.
";


    //  ###############    1 - 2    #########################################################################################################
    // Texte au démarrage
    static public string story12_start = @"Tout le monde est mort. Vous regardez autour de vous ne voyant que des corps sans vie, le sol recouvert du sang de vos victimes dont l’odeur vous brûle les narines. Vous avancez calmement vers la porte. Une fois la première barrière à votre liberté détruite, vous continuez votre chemin. Après avoir survécu, votre envie de sortir n’en est que plus grande. Jusqu’où cette force qui vous fait avancer vous emmènera ?

En marchant, vous remarquez un scientifique encore en vie, seul survivant de votre combat. Vous vous approchez de lui lentement. Il vous remarque et rampe pour tenter de s’échapper. Une fois à son niveau, il se retourne, en pleurs.
 
“Pitié, me tue pas ! J’ai fait que suivre les ordres ! Je veux pas mourir ! ”

Vous le regardez, silencieux, vous demandant quoi faire.
";

    // Choix 1A
    static public string story12_1A = @"Ne ressentant d’animosité de sa part, nul besoin de prendre sa vie. Vous sentez un flot de tranquillité vous parcourir. 

“Si tu tiens à la vie, ne recroise jamais ma route.”

Le scientifique se relève apeuré et se met à courir en pleurant. Dans la précipitation, il fait tomber un badge de sa poche, sûrement une clé d’accès. Vous vous baissez afin de la ramasser avant de reprendre votre route. 

Vous allez pour sortir de la salle quand dans une cellule semblable à la vôtre, un prisonnier s’approche des barreaux.

“Hey ! Fais moi sortir ! Je peux t’aider !”

Un autre prisonnier… Comme vous, il a dû subir de lourdes modifications. Est-il vraiment prudent de le laisser sortir ? Que faire ? 
";

    // Choix 1B
    static public string story12_1B = @"Les souvenirs d’expériences refont surface, un flot d’animosité et de haine monte en vous.
 
“Vous devez tous mourir pour ce que vous m’avez fait !”

Le regard sombre, vous prenez votre couteau et d’un geste rapide tranchez la gorge du scientifique qui n’a pas eu le temps de réagir. Ses yeux vous fixent, terrifiés, tandis que la vie le quitte peu à peu. Une fois mort, vous remarquez un badge dans sa poche, sûrement une clé d’accès. Vous vous baissez afin de la ramasser avant de reprendre votre route. 

Vous passez les portes et arrivez dans un long couloir dans lequel vous avancez. Sur votre route, vous passez devant une cellule semblable à la votre. Le prisonnier vous voit et s’approche de la porte.

“Hey ! Fais moi sortir !  Je peux t’aider ! “
 
Un autre prisonnier… Comme vous, il a dû subir de lourdes modifications. Est-il vraiment prudent de le laisser sortir ? 
";

    // Choix 2A
    static public string story12_2A = @"Vous vous avancez vers la porte de la cellule. Vous sortez le badge de votre poche et le passez dans le scanneur. Après quelques secondes, la porte s’ouvre. Le prisonnier sort lentement de la cellule et vous regarde.

“Merci ! Je n’oublierais pas ce que tu viens de faire. Pour te remercier, je peux te dire qu’il y a une salle d'armes un peu plus loin après la salle de contrôle. Tu y trouveras peut-être quelque chose ! Moi j’ai encore un truc à faire avant de sortir. On se retrouvera peut-être ! Bon courage.” 

Sans attendre il court et disparaît. Mais les informations qu’il vous a donné seront sûrement utiles. Vous reprenez votre route jusqu’à arriver devant la porte. Vous sentez derrière cette porte beaucoup de scientifiques et de militaires. Un nouveau massacre se prépare... 
";

    // Choix 2B
    static public string story12_2B = @"Mieux vaut ne pas le libérer. Il pourrait être dangereux. Vous reprenez votre route sans porter attention au prisonnier. Mais ce dernier se met à crier. 

“Attends ! Reviens ! On est pareil toi et moi ! Moi aussi je veux sortir ! S’il te plait ! NE ME LAISSE PAS ! TU VAS LE REGRETTER ! JE VAIS TE TUER ! ” 

Vous continuez votre route en le laissant seul. Ses cris sont de plus en plus étouffés au fur et à mesure que vous avancez. Une fois devant la porte, vous sentez derrière la présence de beaucoup de scientifiques et de militaires. Un nouveau massacre se prépare... 
";


    //  ###############    2 - 3    #########################################################################################################
    // Texte au démarrage de l'histoire 2 - 3
    static public string story23_start = @"Encore une fois, tout le monde est mort. Vous avancez vers la sortie, laissant derrière vous les traces d’un violent massacre. Encore une fois, vous avancez dans un long couloir sombre. Pendant votre marche, vous arrivez devant une porte sur laquelle est écrit “Salle de contrôle”. 

Vous décidez d’entrer. Dans la salle, vous remarquez une console. Celle-ci gère la vidéo-surveillance du laboratoire. Vous remarquez qu’elle est reliée à une grande armoire dans laquelle sont stockées des disques durs, sûrement pour stocker les archives vidéo. Ces informations pourraient être compromettantes pour le laboratoire… ou pour vous… 

Vous hésitez entre les détruire ou les prendre… Qu’allez-vous faire ? 
";

    // Choix 1A FREE
    static public string story23_1A_free = @"Ces preuves pourraient être une monnaie d’échange contre votre liberté. 

Vous décidez de prendre les disques. Une fois toutes les preuves prises, vous vous souvenez de ce qu’avait dit le prisonnier à propos d’une salle d'armes après la salle de contrôle. Vous reprenez donc votre route en quête de cette salle. 

Une fois trouvée, vous y entrez. Malheureusement la salle est vide. Toutes les armes ont été prises. Vous continuez de chercher en fouillant dans les casiers jusqu’à trouver des chargeurs. Ces chargeurs semblent plus grands et semblent contenir des balles plus puissantes. Vous les prenez avant de continuer votre route. Au bout du couloir, une nouvelle porte vous fait face. Comme la dernière, vous sentez la présence d’ennemis. 

Êtes-vous prêt pour un nouveau massacre ?
";

    // Choix 1A LEAVE
    static public string story23_1A_leave = @"Ces preuves pourraient être une monnaie d’échange contre votre liberté. 

Vous décidez de prendre les disques. Une fois toutes les preuves prises, vous ressortez de la salle, reprenant votre route. Au bout du couloir, une nouvelle porte vous fait face. Comme la dernière, vous sentez la présence d’ennemis. 

Êtes-vous prêt pour un nouveau massacre ? 
";

    // Choix 1A FREE
    static public string story23_1B_free = @"Personne ne doit savoir… Vous levez votre arme et tirez dans l’armoire afin de la détruire. Une fois toutes les preuves détruites, vous vous souvenez de ce qu’avait dit le prisonnier à propos d’une salle d'armes après la salle de contrôle. Vous reprenez donc votre route en quête de cette salle. 

Une fois trouvée, vous y entrez. Malheureusement la salle est vide. Toutes les armes ont été prises. Vous continuez de chercher en fouillant dans les casiers jusqu’à trouver des chargeurs. Ces chargeurs semblent plus grands et semblent contenir des balles plus puissantes. Vous les prenez avant de continuer votre route. Au bout du couloir, une nouvelle porte vous fait face. Comme la dernière, vous sentez la présence d’ennemis. 

Êtes-vous prêt pour un nouveau massacre ? 
";

    // Choix 1B LEAVE
    static public string story23_1B_leave = @"Personne ne doit savoir… Vous levez votre arme et tirez dans l’armoire afin de la détruire. Une fois toutes les preuves détruites, vous ressortez de la salle, reprenant votre route. Au bout du couloir, une nouvelle porte vous fait face. Comme la dernière, vous sentez la présence d’ennemis. 

Êtes-vous prêt pour un nouveau massacre ? 
";


    //  ###############    3 - 4    #########################################################################################################
    //          ####### LOUP - GAROU #######
    // Texte au démarrage
    static public string story34LG_start = @"Votre humanité disparaît encore un peu laissant place à une bestialité violente. 

Vous vous ruez vers la sortie, courant à toute vitesse dans le couloir. Vous vous arrêtez soudainement, sentant des présences derrière une porte du couloir. Vous ne sentez aucune animosité de leur part, seulement de la peur. 

Vous hésitez à nouveau… Entrer dans la salle et tuer les ennemies ou continuez votre route.
";

    // Choix 1A
    static public string story34LG_1A = @"Ils ne semblent pas être une menace. Votre objectif est de sortir d’ici. 

Vous reprenez votre course, décidé à en finir avec ce laboratoire. Vous avancez jusqu’à tomber sur une nouvelle porte, sentant des ennemis approcher. 

Cette fois-ci, impossible de les esquiver…  
";

    // Choix 1B
    static public string story34LG_1B = @"L’envie de tuer est trop présente… 

Vous défoncez la porte, découvrant vos ennemis, apeurés, cachés dans la salle de repos. La porte se ferme, les cris commencent mais ne durent que quelques secondes. La porte se rouvre et vous sortez, les griffes en sang, cherchant de nouvelles proies. Vous avancez jusqu’à tomber sur une porte, sentant des ennemis approcher.  

Un nouveau massacre se prépare !
";

    //       ####### HUMAIN / JEUNE LOUP #######
    // Texte au démarrage
    static public string story34HJL_start = @"Le massacre est terminé… Vous êtes épuisé par les combats. L’odeur du sang et la vision des cadavres qui s’empilent vous met mal à l’aise encore une fois. 

Vous avancez calmement vers la sortie. Vous ouvrez la porte et remarquez encore un long couloir. Vous vous enfoncez dans ce couloir jusqu’à tomber sur une porte. Vous ouvrez la porte discrètement. C’est une salle de repos pour les scientifiques. 

Vous trouverez peut-être quelque chose dedans. Que faire ? 
";

    // Choix 1A
    static public string story34HJL_1A = @"Vous décidez d’entrer. Vous vous enfoncez dans la salle de repos jusqu’à trouver un vestiaire dans lequel se trouvent de nombreux casiers. Vous décidez de fouiller dans les casiers afin de trouver quelque chose. Après plusieurs casiers, vous trouvez une mallette noire. 

Vous ouvrez la mallette et y trouvez un détonateur. Ce dernier semble pouvoir s’activer grâce à une empreinte. Vous vous demandez à quoi il peut bien servir. Il semble important, cela pourrait être utile. Vous le prenez et continuez votre fouille. Malheureusement, à part quelques barres de chocolats que vous mangez directement, vous ne trouvez rien d’autre d’utile. 

Vous ressortez donc de la salle et refermez la porte avant de continuer votre route dans le couloir, décidé à en finir avec ce laboratoire. Vous avancez jusqu’à tomber sur une nouvelle porte, entendant des ennemis approcher. 

Encore une épreuve à passer pour espérer pouvoir sortir…  
";

    // Choix 1B
    static public string story34HJL_1B = @"Votre objectif est de sortir d’ici. 

Vous refermez donc la porte et continuez de marcher dans le couloir, décidé à en finir avec ce laboratoire. Vous avancez jusqu’à tomber sur une nouvelle porte, entendant des ennemis approcher. 

Encore une épreuve à passer pour espérer pouvoir sortir…  
";


    //  ###############    4 - 5    #########################################################################################################
    //             ####### ALPHA #######
    // Texte au démarrage
    static public string story45A_start = @"Votre humanité disparaît encore un peu laissant place à une bestialité violente. 

Vous vous ruez vers la sortie, courant à toute vitesse dans le couloir. Vous vous arrêtez soudainement, sentant des présences derrière une porte.  

Vous défoncez la porte, découvrant vos ennemis, apeurés, cachés dans la salle de repos. La porte se ferme, les cris commencent mais ne durent que quelques secondes. La porte se rouvre et vous sortez, les griffes en sang, cherchant de nouvelles proies. Vous avancez jusqu’à tomber sur une porte, sentant des ennemis approcher. 

Un nouveau massacre se prépare !
";

    //          ####### LOUP - GAROU #######
    // Texte au démarrage
    static public string story45LG_start = @"Une fois tout le monde mort, vous examinez plus en détails les salles du couloir. Celles-ci semblent être des laboratoires. 

Vous remarquez dans l’une des salles, une armoire pleine d’échantillons. Ces échantillons ne vous sont pas anodins puisqu’ils ressemblent à ceux que les scientifiques vous injectent pendant les expériences. Ils semblent être la source de votre capacité à vous transformer. Des souvenirs d’expériences horribles vous revienne. 

Peut-être devriez-vous les détruire ? Cela évitera peut-être à d'autres prisonniers de souffrir comme vous avez souffert…
";

    // Choix 1A
    static public string story45LG_1A = @"“Personne ne doit vivre ça…” 

Vous avancez vers l’armoire avant de donner de puissants coups de griffes sur celle-ci. Peu de temps suffit avant que tous les échantillons de l’armoire soient complètement détruits. Maintenant que tout est détruit, vous sortez de la salle, reprenant votre course. Sur la route, vous remarquez un plan sur un mur. En l’examinant, vous remarquez qu’il ne reste plus que quelques salles. 

Vous touchez au but. 

Votre cauchemar est bientôt terminé. Mais pas le temps de se réjouir… Vous sentez dans la prochaine salle les militaires se mettre en position. Vous vous approchez de la porte, prêt à l’ouvrir.
";

    // Choix 1B
    static public string story45LG_1B = @"“C’est plus mon problème…” 

Vous vous retournez et sortez de la pièce vous dirigeant vers la prochaine salle. Votre seule envie est de sortir d’ici. Sur la route, vous remarquez un plan sur un mur. En l’examinant, vous remarquez qu’il ne reste plus que quelques salles. 

Vous touchez au but. 

Votre cauchemar est bientôt terminé. Mais pas le temps de se réjouir… Vous sentez dans la prochaine salle les militaires se mettre en position. Vous vous approchez de la porte, prêt à l’ouvrir.
";

    //       ####### HUMAIN / JEUNE LOUP #######
    // Texte au démarrage
    static public string story45HJL_start = @"Une fois tout le monde mort, vous examinez plus en détails les salles du couloir. Celles-ci semblent être des laboratoires. Vous remarquez dans l’une des salles, une armoire pleine d’échantillons. Ces échantillons ne vous sont pas anodins puisqu’ils ressemblent à ceux que les scientifiques vous injectent pendant les expériences. Ils semblent être la source de votre capacité à vous transformer. Des souvenirs d’expériences horribles vous revienne. 

Peut-être devriez-vous les détruire ? Cela évitera peut-être à d'autres prisonniers de souffrir comme vous avez souffert…
";

    // Choix 1A
    static public string story45HJL_1A = @"“Personne ne doit vivre ça…” 

Vous avancez vers l’armoire et l’ouvrez pour prendre les flacons un à un et les exploser au sol. Après les avoir tous détruits, vous remarquez sur le bureau un dernier flacon. Vous approchez du bureau pour le détruire lui aussi mais avant de le prendre, vous remarquez qu’un bloc-note se trouve à côté. Vous le prenez et commencez à lire les notes écrites. 

“Test X15-B3 :  :  Sujet 39 (mort)
Le mélange des échantillons 11 et 56 a donné de très bons résultats. Le sujet atteint sa forme finale quelques secondes après ingestion. Un vrai monstre. Toutefois, le sujet n’a pas supporté la transformation et est mort peu de temps après. Je pense qu’une transformation divisée est nécessaire afin d’éviter un changement trop brutal. De nouveaux tests seront faits afin de déterminer la division minimale à effectuer.  Mais je pense être en bonne voie au vu des résultats du sujet 4-”

Le scientifique n’a manifestement pas eu le temps de finir. Vous reposez le bloc note et prenez en main le flacon. Que faire… En le buvant vous obtiendrez sûrement une force incroyable. 

Mais à quel prix ? Que faire ?
";

    // Choix 1B
    static public string story45HJL_1B = @"“C’est plus mon problème…” 

Vous vous retournez et sortez de la pièce vous dirigeant vers la prochaine salle. 

Votre seule envie est de sortir d’ici. 

En marchant vous remarquez un plan sur un mur. En l’examinant, vous remarquez qu’il ne reste plus que quelques salles. 

Vous touchez au but. 

Votre cauchemar est bientôt terminé. Mais pas le temps de se réjouir… Vous entendez dans la prochaine salle les militaires se mettre en position. Vous vous approchez de la porte, prêt à l’ouvrir.
";

    // Choix 2A
    static public string story45HJL_2A = @"“Je veux pas être ce monstre…” 

Vous lâchez le flacon qui explose au sol. Vous déchirez la feuille du bloc-note avant de sortir de la salle. 

Vous avancez dans le couloir en examinant les autres salles afin de détruire chaque échantillon présent. Une fois tous les échantillons détruits, vous reprenez votre route. En marchant vous remarquez un plan sur un mur. En l’examinant, vous remarquez qu’il ne reste plus que quelques salles. 

Vous touchez au but. 

Votre cauchemar est bientôt terminé. Mais pas le temps de se réjouir… Vous entendez dans la prochaine salle les militaires se mettre en position. Vous vous approchez de la porte, prêt à l’ouvrir. 
";

    // Choix 2B JEUNE LOUP
    static public string story45JL_2B = @"“J’ai besoin de cette force…” 

Vous devez sortir... ou mourir… Vous commencez à boire le contenu du flacon. Après quelques gorgées, une douleur se fait sentir. Vous reculez, lâchant le flacon qui explose au sol. 

Vous sentez votre corps se transformer à nouveau. Vous devenez plus grand et plus imposant, vos griffes poussent et n'arrêtent pas de grandir, vos sens deviennent encore plus affûtés. Mais cette fois-ci la transformation ne semble pas être que physique… Votre esprit se trouble, peu à peu vous n’arrivez plus à réfléchir. Les derniers signes de votre humanité semblent disparaître. Vos cris se transforment en hurlement, vos pupilles deviennent de plus en plus rouges. Après de longues secondes de souffrance, la transformation est terminée.  

Votre humanité a complètement disparu pour laisser place à une bestialité violente. Vous êtes devenu une bête sans raison assoiffée de sang. Sentant de nouvelles proies, vous poussez un hurlement avant de vous ruez vers celles-ci. Voilà la force que vous souhaitiez acquérir. 

Malheureusement, cette force est au prix de votre humanité… 
";

    // Choix 2B HUMAIN
    static public string story45H_2B = @"“J’ai besoin de cette force…” 

Vous devez sortir... ou mourir… Vous commencez à boire le contenu du flacon. Après quelques gorgées, une douleur se fait sentir. Vous reculez, lâchant le flacon qui explose au sol. 

Vous sentez votre corps se transformer. Vous devenez plus grand et plus imposant, des griffes poussent et n'arrêtent pas de grandir, vos sens deviennent plus affûtés. Après de longues secondes à souffrir, la douleur s’arrête. Vous restez quelque temps accroupis essayant de reprendre votre souffle et votre calme, constatant que votre corps avait changé. Vous levez la tête et remarquez que votre vision s’est améliorée. 

Vous comprenez maintenant que les expériences qui ont été faites sur vous vous ont changés. Vous pouvez maintenant vous transformer en loup-garou. Mais la transformation n’est pas allée jusqu’au bout et vous le sentez. Peut-être qu’en laissant place à votre bestialité, vous vous transformerez complètement. 

Voilà la force que vous souhaitiez acquérir. Malheureusement, cette force semble être au prix de votre humanité… 
";


    //  ###############    5 - 6    #########################################################################################################
    //             ####### ALPHA #######
    // Texte au démarrage si il a libérer le prisonnier
    static public string story56A_start_free = @"Sans aucun problème, vous avez tué tout le monde. Plus rien ne peut vous arrêter. 

Vous avancez dans la salle vers la sortie, de nouvelles proies attendent. Mais avant d’avoir pu arriver, vous sentez un ennemi plus puissant arriver derrière vous. Vous vous retournez, prêt à attaquer. Il est rapide et violent, les bruits se rapprochent. Après quelques secondes, vous voyez sortir du noir un autre loup-garou. Celui-ci s’arrête devant vous. Son odeur vous rappelle quelque chose. 

C’est le prisonnier que vous avez libéré. Il ne semble pas agressif. Il fait un pas, et range ses crocs et griffe, signe qu’il ne souhaite pas combattre. Au contraire, il semble attendre quelque chose de vous comme un ordre. 

Vous pourriez le recruter. Il semble presque aussi puissant que vous. 

Ou vous pourriez le tuer. N’ayant pas d’intérêt à le recruter. 

Que faire ?  
";

    // Texte au démarrage si il a laisser le prisonnier
    static public string story56A_start_leave = @"Sans aucun problème, vous avez tué tout le monde. Plus rien ne peut vous arrêter. 

Vous avancez dans la salle vers la sortie, de nouvelles proies attendent. Mais avant d’avoir pu arriver, vous sentez un ennemi plus puissant arriver derrière vous. Vous vous retournez, prêt à attaquer. Il est rapide et violent, les bruits se rapprochent. Après quelques secondes, vous voyez sortir du noir un autre loup-garou. Celui-ci s’arrête devant vous. Son odeur vous rappelle quelque chose. 

C’est le prisonnier que vous avez laissé. Il semble vous en vouloir pour ça. Il fait un pas, vous montre ses crocs et ses griffes avant de pousser un hurlement. Effectivement, il veut se venger. 

Pas le choix, il va falloir le tuer pour sortir. 
";

    // Choix 1A
    static public string story56A_1A = @"Vous n’avez pas besoin de lui. Vous êtes assez puissant pour en finir, seul, avec ce laboratoire. Après quelques secondes, vous sortez vos crocs. Signe que vous voulez le tuer. Il comprend ce geste et se met en position, décidé à ne pas mourir. La tension est palpable. L’envie de tuer est oppressante. Ce combat sera le plus violent. Vous hurlez tous les deux, prêt à combattre.    
";

    // Choix 1B
    static public string story56A_1B = @"Vous n’avez pas vraiment besoin de lui. Mais pourquoi le tuer ? Ne trouvant pas de raisons, vous vous tournez pour reprendre votre route, le recrutant. Ensemble, vous continuez vers la porte. 

Ce prochain combat semble être le dernier. Plus rien ne peut vous arrêter. Votre objectif sera bientôt rempli.      
";

    //          ####### LOUP - GAROU #######
    // Texte au démarrage si il a libérer le prisonnier
    static public string story56LG_start_free = @"Non sans mal, vous avez tué tout le monde. Vous rapprochant un peu plus de la sortie. 

Vous avancez dans la salle vers la sortie, de nouvelles proies attendent. Mais avant d’avoir pu arriver, vous sentez un ennemi plus puissant arriver derrière vous. Vous vous retournez, prêt à attaquer. Il est rapide et violent, les bruits se rapprochent. Après quelques secondes, vous voyez sortir du noir un autre loup-garou. Celui-ci s’arrête devant vous. Son odeur vous rappelle quelque chose. 

C’est le prisonnier que vous avez libéré. Il ne semble pas agressif pour l’instant. Mais il semble attendre quelque chose de vous. Comme si il attendait que vous le rejoigniez. Vous ne semblez pas avoir beaucoup d’options. 

Vous pourriez le rejoindre. Sa force pourrait vous aider. Ou vous pourriez le tuer. Empêchant ce monstre sortir d’ici. 

Que faire ?   
";

    // Texte au démarrage si il a laisser le prisonnier
    static public string story56LG_start_leave = @"Non sans mal, vous avez tué tout le monde. Vous rapprochant un peu plus de la sortie. 

Vous avancez dans la salle vers la sortie, de nouvelles proies attendent. Mais avant d’avoir pu arriver, vous sentez un ennemi plus puissant arriver derrière vous. Vous vous retournez, prêt à attaquer. Il est rapide et violent, les bruits se rapprochent. Après quelques secondes, vous voyez sortir du noir un autre loup-garou. Celui-ci s’arrête devant vous. Son odeur vous rappelle quelque chose. 

C’est le prisonnier que vous avez laissé. Il semble vous en vouloir pour ça. Il fait un pas, vous montre ses crocs et ses griffes avant de pousser un hurlement. Effectivement, il veut se venger. 

Pas le choix, il va falloir le tuer pour sortir. 
";

    // Choix 1A
    static public string story56LG_1A = @"Vous ne pouvez pas laisser ce monstre sortir. Cela pourrait causer beaucoup de dégâts. C’est une menace qu’il faut exterminer. Après quelques secondes, vous sortez vos crocs. Signe que vous voulez le tuer. Il comprend ce geste et se met en position, décidé à ne pas mourir. La tension est palpable. L’envie de tuer est oppressante. Ce combat sera le plus violent. Vous hurlez tous les deux, prêt à combattre.    
";

    // Choix 1B
    static public string story56LG_1B = @"Vous avez besoin de sa force. Grâce à elle, vous pourrez sortir d’ici. Vous faites un pas, rangeant vos crocs, signe que vous acceptez de le rejoindre. Il comprend et range à son tour ses crocs avant d’avancer. Vous le suivez et ensemble, vous continuez vers la porte. Ce prochain combat semble être le dernier. Plus rien ne peut vous arrêter. Votre objectif sera bientôt rempli.   
";

    //       ####### JEUNE LOUP #######
    // Texte au démarrage si il a libérer le prisonnier
    static public string story56JL_start_free = @"Non sans mal, vous avez tué tout le monde. Vous rapprochant un peu plus de la sortie. 

Vous avancez dans la salle vers la sortie, de nouvelles proies attendent. Mais avant d’avoir pu arriver, vous sentez un ennemi plus puissant arriver derrière vous. Vous vous retournez, prêt à attaquer. Il est rapide et violent, les bruits se rapprochent. Après quelques secondes, vous voyez sortir du noir un autre loup-garou. Celui-ci s’arrête devant vous. Son odeur vous rappelle quelque chose. 

C’est le prisonnier que vous avez libéré. Il ne semble pas vouloir vous tuer mais il a l’air bien décidé à sortir d’ici. Vous ne sentez en lui aucune humanité. Il n’est plus qu’un monstre avide de sang, sans raison. 

Le laisser sortir pourrait causer des dégâts. Mais le combattre pourrait causer votre mort. Que faire ?     
";

    // Texte au démarrage si il a laisser le prisonnier
    static public string story56JL_start_leave = @"Non sans mal, vous avez tué tout le monde. Vous rapprochant un peu plus de la sortie. 

Vous avancez dans la salle vers la sortie, de nouvelles proies attendent. Mais avant d’avoir pu arriver, vous sentez un ennemi plus puissant arriver derrière vous. Vous vous retournez, prêt à attaquer. Il est rapide et violent, les bruits se rapprochent. Après quelques secondes, vous voyez sortir du noir un autre loup-garou. Celui-ci s’arrête devant vous. Son odeur vous rappelle quelque chose. 

C’est le prisonnier que vous avez laissé. Il semble vous en vouloir pour ça. Il fait un pas, vous montre ses crocs et ses griffes avant de pousser un hurlement. Effectivement, il veut se venger. 

Pas le choix, il va falloir le tuer pour sortir. 
";

    // Choix 1A
    static public string story56JL_1A = @"Vous ne pouvez pas laisser ce monstre sortir. Cela pourrait causer beaucoup de dégâts. C’est une menace qu’il faut exterminer. Après quelques secondes, vous sortez vos crocs. Signe que vous voulez le tuer. Il comprend ce geste et se met en position, décidé à ne pas mourir. La tension est palpable. L’envie de tuer est oppressante. Ce combat sera le plus violent. Vous hurlez tous les deux, prêt à combattre.    
";

    // Choix 1B
    static public string story56JL_1B = @"Vous avez besoin de sa force. Grâce à elle, vous pourrez sortir d’ici. Vous faites un pas, rangeant vos crocs, signe que vous ne souhaitez pas le tuer. Il comprend et range à son tour ses crocs avant d’avancer, le laissant passer. Il avance vers la porte et l’enfonce. Vous entendez dans la salle des cris et des coups de feu. Le combat semble violent et sanglant. Mais peu de temps après, le silence regagne la salle. Le combat est terminé. Tout le monde est mort. 
";

    //       ####### HUMAIN #######
    // Texte au démarrage si il a libérer le prisonnier
    static public string story56H_start_free = @"Non sans mal, vous avez tué tout le monde. Vous rapprochant un peu plus de la sortie. 

Vous avancez dans la salle vers la sortie, de nouvelles proies attendent. Mais avant d’avoir pu arriver, vous sentez un ennemi plus puissant arriver derrière vous. Vous vous retournez, prêt à attaquer. Il est rapide et violent, les bruits se rapprochent. Après quelques secondes, vous voyez sortir du noir un autre loup-garou. Celui-ci s’arrête devant vous. Son odeur vous rappelle quelque chose. 

C’est le prisonnier que vous avez libéré. Il ne semble pas vouloir vous tuer mais il a l’air bien décidé à sortir d’ici. Vous ne sentez en lui aucune humanité. Il n’est plus qu’un monstre avide de sang, sans raison. 

Le laisser sortir pourrait causer des dégâts. Mais le combattre pourrait causer votre mort. Que faire ?     
";

    // Texte au démarrage si il a laisser le prisonnier
    static public string story56H_start_leave = @"Non sans mal, vous avez tué tout le monde. Vous rapprochant un peu plus de la sortie. 

Vous avancez dans la salle vers la sortie, de nouvelles proies attendent. Mais avant d’avoir pu arriver, vous sentez un ennemi plus puissant arriver derrière vous. Vous vous retournez, prêt à attaquer. Il est rapide et violent, les bruits se rapprochent. Après quelques secondes, vous voyez sortir du noir un autre loup-garou. Celui-ci s’arrête devant vous. Son odeur vous rappelle quelque chose. 

C’est le prisonnier que vous avez laissé. Il semble vous en vouloir pour ça. Il fait un pas, vous montre ses crocs et ses griffes avant de pousser un hurlement. Effectivement, il veut se venger. 

Pas le choix, il va falloir le tuer pour sortir. 
";

    // Choix 1A
    static public string story56H_1A = @"Vous ne pouvez pas laisser ce monstre sortir. Cela pourrait causer beaucoup de dégâts. C’est une menace qu’il faut exterminer. Après quelques secondes, vous pointez votre arme vers lui. Signe que vous voulez le tuer. Il comprend ce geste et se met en position, décidé à ne pas mourir. La tension est palpable et son envie de tuer est oppressante. Ce combat sera le plus violent. Vous l’entendez hurler, prêt à combattre.    
";

    // Choix 1B
    static public string story56H_1B = @"Vous avez besoin de sa force. Grâce à elle, vous pourrez sortir d’ici. Vous faites un pas de côté, rangeant votre arme, signe que vous ne souhaitez pas le tuer. Il comprend et range ses crocs avant d’avancer, le laissant passer. Il avance vers la porte et l’enfonce. Vous entendez dans la salle des cris et des coups de feu. Le combat semble violent et sanglant. Mais peu de temps après, le silence regagne la salle. Le combat est terminé. Tout le monde est mort. 
";


    //  ###############    COMBAT    #########################################################################################################
    //             ####### ALPHA #######
    // Texte du combat version ALPHA
    static public string storyCombatA = @"Le combat commence, vous sautez tous les deux, crocs et griffes sortis, prêt à tuer. Le prisonnier donne un premier coup de griffe violent que vous esquivez avant de contrer, griffant son visage. 

Celui-ci recule, blessé par ce coup. Sans attendre, vous vous ruez sur lui. Vous êtes plus rapide et plus puissant que lui, ça ne fait aucun doute. Avant qu’il ait pu se protéger ou esquiver, vos crocs se retrouvent enfoncés dans son cou. Il pousse un hurlement et vous repousse. Affaibli, il pousse un hurlement avant de se ruer sur vous. Encore une fois, vous esquivez son attaque avant de porter le coup fatal. Vos griffes viennent se planter là où vos crocs avaient affaibli son cou. La puissance et la vitesse du coup vont jusqu’à décapiter la tête du loup garou. 

Son corps tombe au sol, il est mort. Tandis que sa tête chute, vous poussez un hurlement puissant, signe de votre victoire. Mais votre soif de sang, elle, n’est pas assouvie. Vous sentez de nouvelles proies approcher. Vous vous retournez et courrez vers la prochaine porte. 

Tuer est devenu votre seule raison...  
";

    // Texte du combat version loup garou
    static public string storyCombatLG = @"Le combat commence, vous sautez tous les deux, crocs et griffes sortis, prêt à tuer. Le prisonnier donne un premier coup de griffe violent que vous esquivez avant de contrer, griffant son visage. 

Celui-ci recule, blessé par ce coup. Sans attendre, vous vous ruez sur lui. Il esquive votre coup et vous projette en arrière vous griffant le bras. Il semble aussi puissant que vous. Il hurle avant de se ruer sur vous. Vous vous préparez à esquiver le coup, guettant le bon moment pour lui asséner le coup fatal. Les coups violents s'enchaînent. Vous continuez d'esquiver les coups portant quelques contres le blessant petit à petit. Affaiblis, vous reculez tous les deux. Il pousse un hurlement violent, signe que la prochaine attaque sera la dernière. Il se rue sur vous tentant de planter ses crocs dans votre cou. De justesse vous esquivez, avant de le plaquer au sol et, avant même qu’il ait pu se relever, vos griffes viennent se planter au niveau de son cœur. 

C’est fini… Ce combat vous aura poussé à votre maximum… Vous retirez vos griffes et reculez, épuisé par le combat. Mais pas le temps de reprendre des forces, vous sentez des ennemis approcher. 

Vous reprenez votre route vers la porte, prêt à en finir avec ce laboratoire.
";

    // Texte du combat version jeune loup
    static public string storyCombatJL = @"Le combat commence, vous sautez tous les deux, crocs et griffes sortis, prêt à tuer. Le prisonnier donne un premier coup de griffe violent que vous esquivez avant de contrer, griffant son visage. 

Celui-ci recule, blessé par ce coup. Sans attendre, vous vous ruez sur lui. Il esquive votre coup et vous projette en arrière vous griffant le bras. Il semble aussi puissant que vous. Il hurle avant de se ruer sur vous. Vous vous préparez à esquiver le coup, guettant le bon moment pour lui asséner le coup fatal. Les coups violents s'enchaînent. Vous continuez d'esquiver les coups portant quelques contres le blessant petit à petit. Affaiblis, vous reculez tous les deux. Il pousse un hurlement violent, signe que la prochaine attaque sera la dernière. Il se rue sur vous tentant de planter ses crocs dans votre cou. De justesse vous esquivez, avant de le plaquer au sol et, avant même qu’il ait pu se relever, vos griffes viennent se planter au niveau de son cœur. 

C’est fini… Ce combat vous aura poussé à votre maximum… Vous retirez vos griffes et reculez, épuisé par le combat. Mais pas le temps de reprendre des forces, vous sentez des ennemis approcher. 

Vous reprenez votre route vers la porte, prêt à en finir avec ce laboratoire.
";

    // Texte du combat version humain
    static public string storyCombatH = @"Le combat commence, il se rue sur vous. Vous sautez pour esquiver le coup, tirant sur lui. Les balles ne semblent pas lui faire tant de dégâts, ce combat risque d’être compliqué voir impossible. Il charge à nouveau en essayant de vous griffer. De justesse, vous esquivez passant derrière lui. 

A ce moment, vous remarquez dans son coup une blessure. Il a dû être blessé lors d’un combat. Vous levez votre arme et tirez, visant la blessure. La balle réussit à le transpercer ! Il recule et hurle de douleur. Cette faiblesse est votre seule chance de survivre. Mais avant même d’essayer de tirer à nouveau, il se rue sur vous et donne un coup sur votre bras. Vous êtes projeté en arrière, lâchant le pistolet qui tombe un peu plus loin. 

Vous êtes maintenant désarmé. Vous essayez de récupérer votre arme mais avant d’avoir pu la récupérer, le loup garou vous saute dessus. Pour esquiver, vous sautez à l’opposé. Il ne semble pas vouloir vous laisser récupérer votre arme. Pas le choix, il va falloir faire sans. Vous sortez votre couteau et changez votre posture. Le loup hurle et se rue sur vous. Vous vous tenez prêt à esquiver, guettant le bon moment pour porter un coup. Vous esquivez sans relâche ses coups rapides et violents quand, après une esquive, son point faible se retrouve à votre portée. Vous courez vers lui avant de sauter sur lui. Avant qu’il puisse esquiver, vous plantez votre couteau dans la blessure au niveau de son cou. Le couteau s’enfonce entièrement dans son cou, le faisant hurler de douleur. 

Vous vous reculez, laissant le couteau enfoncé avant de courir vers votre arme. Le loup, blessé par votre couteau, tombe au sol hurlant sous la douleur. Vous récupérez votre arme et commencez à viser avant de vider votre chargeur. Une fois la dernière balle tirée, le silence regagne la pièce. Les hurlements ont cessé. Vous vous approchez du loup prudemment pour vérifier qu’il est bel et bien mort. Aucun signe de vie. Vous vous approchez un peu plus afin de récupérer votre couteau avant de reprendre votre route. 

La sortie est proche. La liberté est à portée...
";


    //  ###############    FIN   #########################################################################################################
    //       ####### HUMAIN #######
    // Texte au démarrage si il a tuer le loup et a le detonateur
    static public string storyFinH_start_kill_deto = @"Ça y est, tout le monde est mort. C’est terminé… Plus qu’à sortir du laboratoire. 

Vous êtes enfin libre. 

Vous avancez épuisé, vers la sortie, enjambant les corps sans vie sur votre route. La grande porte du laboratoire est ouverte, vous sentez l’air frais prendre le dessus sur l’odeur du sang. Vous continuez d’avancer. Une fois sur le palier de la porte, vous vous arrêtez. 

Vous regardez dehors, perdu dans vos pensées face à la beauté de cet environnement. Cela faisait des années que vous n’aviez pas vu autre chose que l’intérieur de ce laboratoire. Le laboratoire est entouré d’une large et épaisse forêt éclairée brièvement par la pleine lune. Au loin vous remarquez la pollution lumineuse d’une ville. Des gens pourront peut-être vous aider. 

Vous allez pour faire un pas mais vous remarquez au sol un militaire. Celui-ci ne ressemble pas à tous ceux que vous avez tués. Vous remarquez une feuille qui dépasse de sa poche. Vous vous baissez et prenez la lettre pour la lire. Elle semble avoir été écrite rapidement.  

“Ordre : 

Vous devez capturer ou tuer le sujet à tout prix. Si la mission échoue, faites tout exploser. Le détonateur se trouve dans une mallette noire. Elle a été donnée au scientifique en chef. Nous avons ajouté vos empreintes pour l’activer.

Il ne doit pas sortir !”

Une mallette noire… Comme celle que vous avez trouvée dans la salle de repos.C’est donc un détonateur pour faire exploser le laboratoire.Vous prenez le détonateur en main. Si vous faites exploser le laboratoire, ils penseront que vous êtes mort. Vous serez libre et pourrez reprendre une vie normale. Mais les généraux à l’origine de ce projet resteront libres. Ce laboratoire pourrait servir à les arrêter. Vous hésitez une dernière fois...
";

    //             ####### LE SECRET #######
    // Texte final
    static public string storyFin_LeSecret = @"Détruire le laboratoire est votre seul moyen d’être libre. 

Vous serez mort pour tout le monde. Vous regardez une dernière fois à l’intérieur avant de mettre le détonateur dans la main du militaire. Vous posez et maintenez ses doigts sur le lecteur jusqu’à ce que le détonateur soit débloqué. Une fois fait, vous appuyez sur le bouton avant de vous relever. 

Vous avancez sur le chemin en entendant plusieurs explosions venir du fond du laboratoire. Petit à petit, les explosions se rapprochent, détruisant tout sur leurs passages jusqu’à ce que les derniers explosifs soient activés. 

Vous vous enfoncez dans la forêt, seul, officiellement mort tandis que le laboratoire explose. Personne ne saura jamais ce qu’il s’est passé ici. Vous avez réussi, vous êtes libre. Beaucoup de gens sont morts pour y parvenir. Vous resterez à jamais dans l’ombre, tentant de fuir ces souvenirs qui resteront pour vous hanter jusqu’à votre dernier souffle, en secret. 

Fin.   
";

    //             ####### LA TRAQUE #######
    // Texte final
    static public string storyFin_LaTraque = @"Détruire le laboratoire n’est pas une option. 

Vous lâchez le détonateur et quittez les lieux, laissant derrière vous les traces de vos massacres sanglants. En ne le détruisant pas, vous pourrez prouver que des choses horribles s’y passent. Mais vous serez probablement traqué à vie. Pour l’instant peu importe. Vous devez vous enfuir. Partir loin d’ici en quête d’un peu de repos. Vous disparaissez dans la noirceur de la nuit. Un jour peut-être trouverez-vous un moyen d’arrêter tout ça…

Quelques jours plus tard, l’armée trouve le laboratoire dévasté grâce à un message anonyme expliquant ce qui s’y trouvait. Bien sûr, toutes traces d’expériences ont été détruites. Après quelques recherches de l’armée, ils comprennent rapidement que des gens étaient emprisonnés ici et que l’un d’eux a disparu. Une enquête est ouverte afin de comprendre ce qui se passait dans le laboratoire et de retrouver le prisonnier qui s’est échappé à tout prix. 

La traque commence. 

Fin.
";

    //             ####### LA TRAQUE 2 #######
    // Texte final
    static public string storyFin_LaTraque_2 = @"Ça y est, tout le monde est mort. C’est terminé… Plus qu’à sortir du laboratoire. Tous ces massacres sont terminés. 

Vous êtes enfin libre. 

Vous avancez épuisé, vers la sortie, enjambant les corps sans vie sur votre route. La grande porte du laboratoire est ouverte, vous sentez l’air frais prendre le dessus sur l’odeur du sang. Vous continuez d’avancer. Une fois sur le palier de la porte, vous vous arrêtez. 

Vous regardez dehors, perdu dans vos pensées face à la beauté de cet environnement. Cela faisait des années que vous n’aviez pas vu autre chose que l’intérieur de ce laboratoire. Le laboratoire est entouré d’une large et épaisse forêt éclairée brièvement par la pleine lune. Au loin vous remarquez la pollution lumineuse d’une ville. Des gens pourront peut-être vous aider. 

Non… Votre nouvelle forme ferait fuir tout le monde. Ils essayeront de vous tuer. Vous devez fuir, seul et trouver un jour peut-être le repos. 

Quelques jours plus tard, l’organisation envoie de nouveaux militaires dans le laboratoire. Après quelques recherches, ils comprennent rapidement que vous avez disparu. Ils montent plusieurs équipes afin de vous retrouver. La traque commence.

Fin. 
";

    //             ####### DU SANG SUR LES MAINS #######
    // Texte final
    static public string storyFin_DuSangSurLesMains = @"Ça y est, tout le monde est mort. C’est terminé… Plus qu’à sortir du laboratoire. 

Vous êtes enfin libre. 

Vous avancez épuisé, vers la sortie, enjambant les corps sans vie sur votre route. La grande porte du laboratoire est ouverte, vous sentez l’air frais prendre le dessus sur l’odeur du sang. Vous continuez d’avancer. Une fois sur le palier de la porte, vous vous arrêtez. Vous regardez dehors, perdu dans vos pensées face à la beauté de cet environnement. Cela faisait des années que vous n’aviez pas vu autre chose que l’intérieur de ce laboratoire. Le laboratoire est entouré d’une large et épaisse forêt éclairée brièvement par la pleine lune. Au loin vous remarquez la pollution lumineuse d’une ville. Vous apercevez des traces de sang sur la route. Vous comprenez que le loup-garou s’y dirige. Mais pas le temps de réfléchir, des hélicoptères approchent. Vous courrez, disparaissant dans la noirceur de la nuit. Plus tard, vous apprendrez que le loup-garou que vous avez laissé partir était bien allé dans la ville. Avide de sang, il avait tué des milliers de personnes avant que l’armée n’arrive et ne l’abatte. Vous êtes libre mais à quel prix ? Vous continuez de fuir redoutant un jour d’être rattrapé, du sang sur les mains. 

Fin.
";

    //             ####### LOUP SOLITAIRE #######
    // Texte final
    static public string storyFin_LoupSolitaire = @"Ça y est, tout le monde est mort. C’est terminé… Plus qu’à sortir du laboratoire. 

Vous êtes enfin libre. 

Vous avancez épuisé, vers la sortie, enjambant les corps sans vie sur votre route. La grande porte du laboratoire est ouverte, vous sentez l’air frais prendre le dessus sur l’odeur du sang. Vous continuez d’avancer. Une fois sur le palier de la porte, vous vous arrêtez. Vous regardez dehors, perdu dans vos pensées face à la beauté de cet environnement. Cela faisait des années que vous n’aviez pas vu autre chose que l’intérieur de ce laboratoire. Le laboratoire est entouré d’une large et épaisse forêt éclairée brièvement par la pleine lune. Au loin vous remarquez la pollution lumineuse d’une ville. Impossible d’y aller sous cette forme. Vous regardez encore un peu la pleine lune avant de partir, seul, dans la forêt. Vous trouverez un jour un coin tranquille dans lequel vous pourrez vous installer, chassant des animaux pour survivre. Vous allez vivre mais au prix de la solitude, comme un loup solitaire. 

Fin.
";

    //             ####### LA MEUTE #######
    // Texte final
    static public string storyFin_LaMeute = @"Ça y est, tout le monde est mort. C’est terminé… Plus qu’à sortir du laboratoire. 

Vous êtes enfin libre. 

Vous avancez épuisé, vers la sortie, enjambant les corps sans vie sur votre route. La grande porte du laboratoire est ouverte, vous sentez l’air frais prendre le dessus sur l’odeur du sang. Vous continuez d’avancer. Une fois sur le palier de la porte, vous vous arrêtez. 

Le prisonnier vous rejoint et vous regardez tous deux dehors, perdu dans vos pensées face à la beauté de cet environnement. Cela faisait des années que vous n’aviez pas vu autre chose que l’intérieur de ce laboratoire. Le laboratoire est entouré d’une large et épaisse forêt éclairée brièvement par la pleine lune. Au loin vous remarquez la pollution lumineuse d’une ville. Mais pas le temps de profiter, des hélicoptères approchent. Vous courrez tous les deux, disparaissant dans la noirceur de la nuit. Traqué par des militaires, vous vous enfuyez encore et encore à la recherche d’un endroit tranquille. 

Un endroit où vous pourrez rester tranquille et vivre en meute. 

Fin.
";

    //             ####### L'ALPHA #######
    // Texte final
    static public string storyFin_LAlpha = @"Ça y est, tout le monde est mort. C’est terminé… Plus qu’à sortir du laboratoire. 

Vous êtes enfin libre. 

Mais il vous faut plus de sang. Votre bestialité prend à nouveau le dessus. Vous avancez vers la sortie et, une fois sur le palier de la porte, vous regardez autour de vous. Le laboratoire est entouré d’une large et épaisse forêt éclairée brièvement par la pleine lune. Au loin vous remarquez la pollution lumineuse d’une ville. 

Vous regardez la pleine lune. 

Votre envie de tuer augmente. Vous poussez un hurlement qui résonne dans la forêt avant de vous ruer vers la ville. Plus rien ne peut vous arrêter. Vous êtes devenus un monstre. Durant des heures, la ville est mise à feu et à sang. Les cris résonnent et le sang recouvre peu à peu les rues. Après des milliers de victimes, l’armée arrive enfin dans la ville. Elle met de longues minutes et beaucoup de balles avant de réussir à vous tuer. Vous vous écroulez au sol, dans le sang de vos victimes. Ce sang sera la dernière chose que vous verrez. 

Fin.
";

    //             ####### L'ARMAGEDDON #######
    // Texte final
    static public string storyFin_LArmageddon = @"Ça y est, tout le monde est mort. C’est terminé… Plus qu’à sortir du laboratoire. 

Vous êtes enfin libre. 

Mais il vous faut plus de sang. Votre bestialité prend à nouveau le dessus. Vous avancez vers la sortie et, une fois sur le palier de la porte, l’autre loup-garou vous rejoint et vous regardez tous deux autour de vous. Le laboratoire est entouré d’une large et épaisse forêt éclairée brièvement par la pleine lune. Au loin vous remarquez la pollution lumineuse d’une ville. 

Vous regardez la pleine lune. 

Vos envies de tuer augmentent. Vous poussez un hurlement qui résonne dans la forêt avant de vous ruer vers la ville. Plus rien ne peut vous arrêter. Vous êtes devenus des monstres. Durant des heures, la ville est mise à feu et à sang. Les cris résonnent et le sang recouvre peu à peu les rues. A deux, vous êtes invincibles. Même l’armée, impuissante, ne peut vous arrêter. 

Jusqu’où ira le massacre? 

Fin.   
";
}

