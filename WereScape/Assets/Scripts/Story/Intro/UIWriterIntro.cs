﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIWriterIntro : MonoBehaviour
{
    private Text StoryText;                               // Variable contenant sur le texte du niveau

    private TextWriter.TextWriterSingle textWriterSingle; // Permet de gérer plusieurs writers en même temps

    public TextWriter writer;                            // Référence sur le text writer pour la musique

    // Références sur les 3 boutons de choix
    public GameObject Choice01;
    public GameObject Choice02;
    public GameObject Choice03;

    private int state;                                     // Permet de savoir où on en est dans la scène

    private void Awake()
    {
        StoryText = transform.Find("Canvas").Find("TransparentPanel").Find("StoryText").GetComponent<Text>();
    }

    private void HideChoices()
    {
        Choice01.SetActive(false);
        Choice02.SetActive(false);
        Choice03.SetActive(false);
    }

    private void ShowChoices()
    {
        Choice01.SetActive(true);
        Choice02.SetActive(true);
    }

    private void ShowChoice()
    {
        Choice03.SetActive(true);
    }

    private void Start()
    {
        Choice03.GetComponentInChildren<Text>().text = "Commencer";
        string message = GlobalStory.intro_start;
        textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoice);
    }

    public void Choice1()
    {
    }

    public void Choice2()
    {
    }

    public void Choice3()
    {
        LoadNextScene();
    }

    public void LoadNextScene()
    {
        writer.FadeToZero();
        //Lancement de la scene suivante
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
