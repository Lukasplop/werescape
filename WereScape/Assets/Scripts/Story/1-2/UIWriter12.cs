﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIWriter12 : MonoBehaviour
{

    public LevelChanger levelChanger;                     // Référence sur le LevelChanger permettant de passer au niveau suivant 
    private Text StoryText;                               // Variable contenant sur le texte du niveau

    private TextWriter.TextWriterSingle textWriterSingle; // Permet de gérer plusieurs writers en même temps

    public TextWriter writer;                            // Référence sur le text writer pour la musique

    // Références sur les 3 boutons de choix
    public GameObject Choice01;
    public GameObject Choice02;
    public GameObject Choice03;

    private int state;                                     // Permet de savoir où on en est dans la scène

    private void Awake()
    {
        StoryText = transform.Find("Canvas").Find("TransparentPanel").Find("StoryText").GetComponent<Text>();
    }

    private void HideChoices()
    {
        Choice01.SetActive(false);
        Choice02.SetActive(false);
        Choice03.SetActive(false);
    }

    private void ShowChoices()
    {
        Choice01.SetActive(true);
        Choice02.SetActive(true);
    }

    private void ShowChoice()
    {
        Choice03.SetActive(true);
    }

    private void Start()
    {
        Choice01.GetComponentInChildren<Text>().text = "L'épargner";
        Choice02.GetComponentInChildren<Text>().text = "Le tuer";
        string message = GlobalStory.story12_start;
        textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoices);
        state = 0;
    }

    public void Choice1()
    {
        if (state == 0)
        {
            HideChoices();
            Choice01.GetComponentInChildren<Text>().text = "Le libérer";
            Choice02.GetComponentInChildren<Text>().text = "Le laisser";
            string message = GlobalStory.story12_1A;
            textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoices);
            ++state;
        }
        else
        {
            GlobalStory.freePrisoner = true;
            HideChoices();
            Choice03.GetComponentInChildren<Text>().text = "Je suis prêt !";
            string message = GlobalStory.story12_2A;
            textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoice);
            ++state;
        }
    }

    public void Choice2()
    {
        if (state == 0)
        {
            GlobalVariables.humanity += 5;
            HideChoices();
            Choice01.GetComponentInChildren<Text>().text = "Le libérer";
            Choice02.GetComponentInChildren<Text>().text = "Le laisser";
            string message = GlobalStory.story12_1B;
            textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoices);
            ++state;
        }
        else
        {
            GlobalVariables.humanity += 5;
            HideChoices();
            Choice03.GetComponentInChildren<Text>().text = "Je suis prêt !";
            string message = GlobalStory.story12_2B;
            textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoice);
            ++state;
        }

    }

    public void Choice3()
    {
        // Texte de transformation si l'humanité atteint un certain seuil
        if(GlobalVariables.humanity >= GlobalVariables.evolutionYoungWolf && 
            GlobalVariables.humanity < GlobalVariables.evolutionWereWolf && 
            GlobalVariables.forme != GlobalVariables.Formes.YoungWolf)
        {
            GlobalVariables.forme = GlobalVariables.Formes.YoungWolf;
            Choice03.GetComponentInChildren<Text>().text = "Continuer";
            string message = GlobalStory.transfo_young;
            textWriterSingle = TextWriter.AddWriter_Static(StoryText, message, .02f, true, true, ShowChoice);
        }
        else
        {
            LoadNextScene();
        }
    }

    public void LoadNextScene()
    {
        writer.FadeToZero();
        //Lancement de la scene suivante
        levelChanger.FadeToNextLevel();
    }
}
