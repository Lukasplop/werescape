﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuManager : MonoBehaviour
{
    public LevelManager levelManager;        // Une référence sur le levelManager pour pouvoir changer de scène

    public void Jouer()
    {
        levelManager.FadeToNextLevel();     // Permet de lancer le jeu 
    }

    public void Quitter()
    {
        Application.Quit();         
    }
}
