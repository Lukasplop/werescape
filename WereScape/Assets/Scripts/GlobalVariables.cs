﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Classe créée pour récupérer des valeurs entre les scènes, apparemment plus efficace que DontDestroyOnLoad()
public class GlobalVariables    
{
    public enum Formes { Human, YoungWolf, WereWolf, ALPHA};    // Une énumération contenant toutes les formes du joueur

    static public Formes forme = Formes.Human;                  // La forme actuelle du joueur
    static public int evolutionYoungWolf = 10;                  // La valeur de l'évolution en jeune loup
    static public int evolutionWereWolf = 17;                   // La valeur de l'évolution en loup garou
    static public int evolutionALPHA = 22;                      // La valeur de l'évolution en loup ALPHA
    static public int humanity = 0;                             // L'humanité du joueur, 0 = tout début

    static public bool chargeurUpgrade = false;                 // Si le chargeur a été amélioré ou non

    static public bool isPaused = false;                        // Si le jeu est en pause ou non
}

